<?php

/**
 * @file
 *
 * Template to display a view as a table.
 *
 * Available variables:
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 *
 * @ingroup templates
 */
?>
<table id="teampeople" class="<?php print($classes); ?>">
  <tr>
    <th>Name</th>
    <th>Email</th>
    <th>Active</th>
    <th>Roles</th>
    <th>Last Access</th>
    <th width="10%">Options</th>
  </tr>
  <?php foreach ($rows as $row_count => $row) { ?>
    <tr <?php if ($row_classes[$row_count]) {
      print 'class="' . implode(' ', $row_classes[$row_count]) . '"';
    } ?>>
      <td><?php print($row['name']); ?></td>
      <td><?php print_r($row['mail']); ?></td>
      <td><?php print($row['status']); ?></td>
      <td><?php print($row['rid']); ?></td>
      <td><?php print($row['access']); ?></td>
      <td>
        <?php if (user_access(TEAMS_PERMISSION_ASSIGN_USER)) { ?>
          <a href="/admin/config/teams/team/users/assign/<?php print($row['uid']); ?>">Assign Teams</a>
        <?php } ?>
      </td>
    </tr>
  <?php } ?>
</table>
