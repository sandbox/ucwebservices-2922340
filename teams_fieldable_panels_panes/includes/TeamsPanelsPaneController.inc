<?php

/**
 * Overrides fieldable panels panes controller functionality
 */
class TeamsPanelsPaneController extends PanelsPaneController {

  public function access($op, $entity = NULL, $account = NULL) {
    if (teams_user_is_superadmin()) {
      return TRUE;
    }

    // Listing FPP's in a Page Builder modal
    if ($op === 'create' && is_string($entity)) {
      return TRUE;
    }

    // access denied when entity is empty
    if ($op !== 'create' && empty($entity)) {
      return FALSE;
    }

    // Check default access in ctools for entity view access
    if ($op === 'view') {
      ctools_include('context');
      return ctools_access($entity->view_access, fieldable_panels_panes_get_base_context($entity));
    }

    // Call the parent access function
    $access = parent::access($op, $entity, $account);
    if ($access === TRUE) {
      // Pass all other access checks to teams fieldable panels panes module
      return teams_fieldable_panels_panes_access($op, $entity, $account);
    }

    return FALSE;
  }
}

