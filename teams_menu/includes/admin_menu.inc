<?php
/**
 * @file
 *
 * Access control alters for admin_menu module.
 */

/**
 * Implements hook_admin_menu_output_alter().
 *
 * Remove links from the Structure => Menu that this user doesn't have permission to access
 *
 * @param $content
 */
function teams_menu_admin_menu_output_alter(&$content) {
  if (teams_user_is_superadmin()) {
    return;
  }

  if (isset($content['menu']['admin/structure']['admin/structure/menu'])) {
    $user_teams = teams_get_user_teams();
    if (empty($user_teams)) {
      $menu_names = [];
    }
    else {
      $menu_names = _teams_menu_find_menu_names_by_teams($user_teams);
    }

    foreach ($content['menu']['admin/structure']['admin/structure/menu'] as $menu_url => $type_menu) {
      if (isset($type_menu['#title']) && strpos($menu_url, '/menu/manage/') !== FALSE) {
        $path = explode("/", trim($menu_url));
        $menu_name = $path[4];

        if (!in_array($menu_name, $menu_names)) {
          unset($content['menu']['admin/structure']['admin/structure/menu'][$menu_url]);
        }
      }
    }
  }
}
