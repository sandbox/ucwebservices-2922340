<?php

/**
 * @file
 *
 * Teams main module api
 * Api functions
 * - get teams
 * - add Team
 * - del Team
 * - add user to Team
 * - del user from Team
 * - get user teams
 * - get user permissions
 */

// Teams Permissions
const TEAMS_PERMISSION_ADMINISTER = 'administer teams';
const TEAMS_PERMISSION_ACCESS_OVERVIEW = 'access teams overview';
const TEAMS_PERMISSION_ADD_TEAM = 'add teams';
const TEAMS_PERMISSION_EDIT_TEAM = 'edit teams';
const TEAMS_PERMISSION_DELETE_TEAM = 'delete teams';
const TEAMS_PERMISSION_BULK_UPDATE = 'access teams bulk update';
const TEAMS_PERMISSION_ASSIGN_USER = 'assign user to team';
const TEAMS_PERMISSION_REMOVE_USER = 'remove user from a team';

// Admin includes used to manage the backend admin options & interface
module_load_include('inc', 'teams', 'includes/forms');
module_load_include('inc', 'teams', 'includes/menu');
module_load_include('inc', 'teams', 'includes/views');

// Functional include files used to manage Teams module operations & permissions
module_load_include('inc', 'teams', 'includes/users');

/**
 * Is the current user logged in
 *
 * @deprecated by user_is_logged_in();
 *
 * @return bool
 */
function teams_is_user() {
  return user_is_logged_in();
}

/**
 * Get the list of teams the currently logged in user has access to
 *
 * @return array
 */
function teams_get_user_teams() {
  if (teams_user_is_superadmin()) {
    $teams = db_select('teams_teams', 't')
      ->fields('t', ['team_id'])
      ->execute()
      ->fetchAll(PDO::FETCH_COLUMN);
  }
  elseif (isset($_SESSION['teams_user']['teams'])) {
    $teams = $_SESSION['teams_user']['teams'];
  }

  if (!empty($teams)) {
    return $teams;
  }

  return [];
}

/**
 * Get the teams for the specified user
 *
 * @return array
 */
function teams_get_teams_by_uid($uid) {
  return db_select('teams_users', 'tu')
    ->fields('tu', ['team_id'])
    ->condition('tu.uid', $uid, '=')
    ->execute()
    ->fetchAll(PDO::FETCH_COLUMN);
}

/**
 * Check access permissions api Function
 * provide an array of teams to check against the users team session for
 * correct access permissions
 *
 * @param array $teams_access The teams to check against for the logged in user
 *
 * @return boolean
 */
function teams_has_permission($teams_access) {
  if (teams_user_is_superadmin()) {
    return TRUE;
  }

  $user_teams = teams_get_user_teams();
  if (!empty($user_teams)) {
    foreach ($teams_access as $team) {
      if (in_array($team->team_id, $user_teams)) {
        return TRUE;
      }
    }
  }

  return FALSE;
}

/**
 * Display and traverse teams in an adjacency hierarchical structure
 * provide hierarchical display based off teams array supplied in views, or any
 * other query returning teams as an array
 *
 * @param array $teams The list of teams
 * @param integer $parent_id The team id to filter on
 * @param string $indentation_text The text/HTML to use to indent child items
 * @param integer $level The recursion depth. Used only for recursion.
 * @param array $return_array The return array. Used only for recursion.
 *
 * @return array
 */
function teams_order_teams($teams, $parent_id = 0, $indentation_text = '<div class="indentation"></div>', $level = 0, $return_array = []) {
  // Loop through each team for display and find parents
  foreach ($teams as $team) {
    // Check for current team hierarchy based off parent_id supplied in the function
    // This is needed to diplay the current parent_id team name along with all team children in the return array
    if ($team['team_id'] == $parent_id) {
      // Indent for team name hierarchy and add to return array
      if ($level > 1) {
        $return_array[$team['team_id']]['name'] = str_repeat($indentation_text, $level - 1) . $team['name'];
      }
      else {
        $return_array[$team['team_id']]['name'] = $team['name'];
      }

      // Add team information into return array
      $return_array[$team['team_id']]['team_id'] = $team['team_id'];
      $return_array[$team['team_id']]['parent_id'] = $team['parent_id'];
      $return_array[$team['team_id']]['status'] = $team['status'];
    }

    // Recall function to find child to parent relationships
    if ($team['parent_id'] == $parent_id) {
      $return_array = teams_order_teams($teams, $team['team_id'], $indentation_text, $level + 1, $return_array);
    }
  }

  return $return_array;
}

/**
 * Get all teams in ordered hierarchical structure
 * By default teams_get_teams will return all teams, by providing a params you
 * may get specify the team_td to start with or the status (all, active,
 * closed)
 *
 * @param string $team_id The team id to filter the hierarchy by. Optional.
 *
 * @return array
 */
function teams_get_teams($team_id = 0) {
  $teams = teams_find_all();
  if (!empty($teams)) {
    return teams_order_teams($teams, $team_id);
  }

  return [];
}

/**
 * Get a list of all teams
 *
 * @return array
 */
function teams_find_all() {
  return db_select('teams_teams', 'tot')
    ->fields('tot', [
      'team_id',
      'name',
      'description',
      'parent_id',
      'status',
      'modified',
      'created',
    ])
    ->orderBy('name', 'ASC')
    ->execute()
    ->fetchAll(PDO::FETCH_ASSOC);
}

/**
 * Find a team by it's id
 *
 * @param string $id The team id
 *
 * @return array
 */
function teams_get_team_by_id($id) {
  return db_select('teams_teams', 'tot')
    ->fields('tot', [
      'team_id',
      'name',
      'description',
      'parent_id',
      'status',
      'modified',
      'created',
    ])
    ->condition('tot.team_id', $id, '=')
    ->execute()
    ->fetchAssoc();
}

/**
 * Implements hook_permission().
 */
function teams_permission() {
  return [
    TEAMS_PERMISSION_ADMINISTER => [
      'title' => t('Administer Teams'),
      'description' => t('Grant all permissions to add/edit/delete, bulk update, and remover usres from Teams.'),
    ],
    TEAMS_PERMISSION_ACCESS_OVERVIEW => [
      'title' => t('Access the Teams overview page'),
      'description' => t('Main Teams admin interface page, used to search and display Teams.'),
    ],
    TEAMS_PERMISSION_ADD_TEAM => [
      'title' => t('Add teams'),
      'description' => t('Grants the ability to add new Teams.'),
    ],
    TEAMS_PERMISSION_EDIT_TEAM => [
      'title' => t('Edit teams'),
      'description' => t('Grant the ability to edit Teams.'),
    ],
    TEAMS_PERMISSION_DELETE_TEAM => [
      'title' => t('Delete teams'),
      'description' => t('Grant the ability to delete Teams.'),
    ],
    TEAMS_PERMISSION_BULK_UPDATE => [
      'title' => t('Access Teams bulk update'),
      'description' => t('Grant access to bulk update Teams.'),
    ],
    TEAMS_PERMISSION_ASSIGN_USER => [
      'title' => t('Assign user to a team'),
      'description' => t('Grant the ability to add users to a Team.'),
    ],
    TEAMS_PERMISSION_REMOVE_USER => [
      'title' => t('Remove user from a team'),
      'description' => t('Grant the ability to remove users from a Team.'),
    ],
  ];
}

/**
 * Assign teams to the currently logged in user
 *
 * @param array $teams The teams to assign
 */
function teams_user_assign_teams($teams) {
  global $_SESSION;

  if ($teams === NULL && isset($_SESSION['teams_user']['teams'])) {
    unset($_SESSION['teams_user']['teams']);
    return;
  }

  $_SESSION = [
    'teams_user' => [
      'teams' => $teams,
    ],
  ];
}

/**
 * Print an Access Denied error and exit
 */
function _teams_access_denied() {
  echo "Access denied";
  drupal_access_denied();
  drupal_exit();
}

/**
 * Is the currently logged in user a Teams superadmin?
 *
 * @return bool
 */
function teams_user_is_superadmin() {
  return user_access(TEAMS_PERMISSION_ADMINISTER);
}

/**
 * Add form elements to any teams form
 *
 * @param $form
 * @param $default_checked_teams
 * @param $module_id
 * @param $weight
 * @param $collapse
 *
 * return $module_id
 */
function teams_add_form_elements(&$form, $default_checked_teams, $module_id, $weight = '', $collapse = FALSE, $disable = FALSE) {
  // Overwrite default checked teams with teams user session
  if (empty($default_checked_teams) && !teams_user_is_superadmin()) {
    $default_checked_teams = teams_get_user_teams();
  }

  $all_teams = _teams_get_all_teams_form();

  $module_id = 'teams_' . $module_id . '_team_select';

  // Attach teams display to form as content
  $form[$module_id] = [
    '#type' => 'fieldset',
    '#title' => t('Teams'),
    '#description' => '<h2>Teams</h2><p><strong>Current teams with access:</strong></p>',
    '#collapsible' => TRUE,
    '#collapsed' => $collapse ? $collapse : FALSE,
    '#group' => 'additional_settings',
    '#weight' => $weight ? $weight : -1000,
  ];

  // Add teams display to form
  $form[$module_id]['teams'] = [
    '#type' => 'checkboxes',
    '#title' => t('Teams'),
    '#options' => $all_teams,
    '#default_value' => $default_checked_teams ? $default_checked_teams : [],
    '#required' => FALSE,
    '#disabled' => $disable ? $disable : FALSE,
  ];

  return $module_id;
}

/**
 * Get all teams for adding to forms
 *
 * @return array
 */
function _teams_get_all_teams_form() {
  $all_teams = [];
  foreach (teams_get_teams() as $team) {
    $all_teams[$team['team_id']] = $team['name'];
  }

  return $all_teams;
}

/**
 * Return a list of team names that the current user belongs to
 *
 * @return mixed
 */
function teams_get_user_teams_by_name() {
  $user_teams = teams_get_user_teams();
  if (!empty($user_teams)) {
    return db_select('teams_teams', 'tt')
      ->fields('tt', ['name'])
      ->condition('tt.team_id', $user_teams, 'IN')
      ->execute()
      ->fetchAll(PDO::FETCH_COLUMN, 0);
  }

  return [];
}
