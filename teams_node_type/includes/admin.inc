<?php
/**
 * @file
 *
 * Admin file for Teams node type module
 */

/**
 * Implements hook_form_alter().
 *
 * Teams node type form alter used to integrate ownership into admin display
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function teams_node_type_form_alter(&$form, &$form_state, $form_id) {
  if (!user_is_logged_in()) {
    return;
  }

  // Make sure this is an node type form and we're not deleting the entity
  if (isset($form['#node_type']) && is_array(['#node_type'])) {
    // Get list of all teams
    $all_teams = [];
    foreach (teams_get_teams() as $team) {
      $all_teams[$team['team_id']] = $team['name'];
    }

    $is_add_page = empty($form['#node_type']->type);
    if ($is_add_page) {
      // Add team select options to node type add/edit admin pages

      $module_id = teams_add_form_elements($form, [], 'node_type');

      // Disable all teams that you are not a part of and should not be able to select
      $user_teams = teams_get_user_teams();
      foreach ($all_teams as $team_id => $team_name) {
        if (!in_array($team_id, $user_teams)) {
          $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
        }
      }

      // On add node type form submit create handler
      // Note: using $form['actions']['submit']['#submit'][] will not give you an id on submit
      // The standard $form['#submit'] will return an id on submit
      $form['#submit'][] = 'teams_node_type_form_submit_handler';
    }
    // On edit form, display teams that own content without edit ability
    elseif (isset($form['#node_type']->type)) {
      // Get the teams that have access rights to this type
      $default_checked_teams = _teams_node_type_find_teams_by_type($form['#node_type']->type, PDO::FETCH_COLUMN);

      $module_id = teams_add_form_elements($form, $default_checked_teams, 'node_type');

      // Only allow the admin user to update teams after node creation
      $is_admin_user = teams_user_is_superadmin();
      if (!$is_admin_user) {
        foreach ($all_teams as $team_id => $team_name) {
          $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
        }
      }
      else {
        $form['#submit'][] = 'teams_node_type_form_submit_handler';

        // Disable all teams that you are not a part of, and should not be able to select
        $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
        foreach ($disabled_teams as $team_id) {
          $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;

          // Add a hidden input for this checkbox so we still get the form value on submit, but it isn't changeable
          if (in_array($team_id, $default_checked_teams)) {
            $form[$module_id]['teams[' . $team_id . ']'] = [
              '#type' => 'hidden',
              '#value' => $team_id,
            ];
          }
        }
      }
    }
  }
}

/**
 * node type form submit
 *
 * @param $form
 * @param $form_state
 */
function teams_node_type_form_submit_handler(&$form, &$form_state) {
  $node_type = $form_state['values']['type'];
  $current_team_ids = _teams_node_type_find_teams_by_type($node_type, PDO::FETCH_COLUMN);

  $submitted_team_ids = [];
  foreach ($form_state['values']['teams'] as $team_id) {
    if ($team_id !== 0) {
      $submitted_team_ids[] = $team_id;
    }
  }

  $removed_team_ids = array_diff($current_team_ids, $submitted_team_ids);
  if (!empty($removed_team_ids)) {
    $result = db_delete('teams_node_type')
      ->condition('team_id', $removed_team_ids, 'IN')
      ->condition('type', $node_type, '=')
      ->execute();

    if ($result === NULL) {
      drupal_set_message(t("Error removing teams from content type"), 'error');
    }
  }

  $added_team_ids = array_diff($submitted_team_ids, $current_team_ids);
  if (!empty($added_team_ids)) {
    $query = db_insert('teams_node_type')->fields(['type', 'team_id', 'modified', 'created']);
    foreach ($added_team_ids as $team_id) {
      $query->values([
                       $node_type,
                       $team_id,
                       REQUEST_TIME,
                       REQUEST_TIME,
                     ]);
    }

    if ($query->execute() === NULL) {
      drupal_set_message(t("Error updating content type team membership"), 'error');
    }
  }

  drupal_set_message(t("Content type team membership updated"));
}

/**
 * Implements hook_node_type_delete().
 *
 * Cleanup Node Types in Teams table when deleted
 *
 * @param object $node The node
 */
function teams_node_type_node_type_delete($node) {
  // Remove all references to deleted node type in teams_node_type table
  db_delete('teams_node_type')
    ->condition('type', $node->type)
    ->execute();
}

/**
 * Implements hook_preprocess_HOOK
 *
 * Modify content types table to remove operations if access is denied
 *
 * @param $variables
 */
function teams_node_type_preprocess_table(&$variables) {
  $is_content_types_structure_page = (current_path() === 'admin/structure/types');
  if ($is_content_types_structure_page) {
    if (teams_user_is_superadmin()) {
      return;
    }

    // Check for operations header in vars
    foreach ($variables['header'] as $header_column_number => $header) {
      // Find correct operations array key value
      if (isset($header['data']) && $header['data'] === 'Operations') {
        $operations_column_number = $header_column_number;
      }
    }

    // remove operations for views without user access rights
    if (isset($operations_column_number)) {
      $user_teams = teams_get_user_teams();
      if (empty($user_teams)) {
        $node_type_teams = [];
      }
      else {
        $node_type_teams = db_select('teams_node_type', 'tnt')
          ->fields('tnt', ['type'])
          ->distinct()// @TODO: This table needs a unique key on team_id, type
          ->condition('tnt.team_id', $user_teams, 'IN')
          ->execute()
          ->fetchAll(PDO::FETCH_COLUMN, 0);
      }

      // Disable all operations for types user has no access to
      foreach ($variables['rows'] as $row_key => $row) {
        // Get the brick machine name from the title
        $machine_name = preg_replace('/^.+Machine name: /', '', $row[0]);
        $machine_name = preg_replace('/\).+$/', '', $machine_name);

        if (!in_array($machine_name, $node_type_teams)) {
          $variables['rows'][$row_key][$operations_column_number]['data'] = '';
          $variables['rows'][$row_key][2]['data'] = '';
          $variables['rows'][$row_key][3]['data'] = '';
          $variables['rows'][$row_key][4]['data'] = '';
        }
      }
    }
  }
}
