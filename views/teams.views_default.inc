<?php

/*
 * @file
 * Handles Default views.
 * // Declare all the .view files in the views subdir that end in .view
 */

// Get all the views which have been exported from the database and saved as files
function teams_views_default_views() {
    $views_directory = drupal_get_path('module', 'teams') . '/views/defaults';
    $files           = file_scan_directory($views_directory, '/.*\.view$/');

    $views = array();
    foreach ($files as $path => $file) {
        require $path;
        $views[$file->name] = $view; // $view is brought from the file included as $path
    }

    return $views;
}
