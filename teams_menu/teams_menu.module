<?php

/**
 * @file
 *
 * Teams menu access module
 */

module_load_include('inc', 'teams_menu', 'includes/admin');
if (module_exists('admin_menu')) {
  module_load_include('inc', 'teams_menu', 'includes/admin_menu');
}

/**
 * Implements hook_menu_alter().
 *
 * @param $items
 */
function teams_menu_menu_alter(&$items) {
  $items['admin/structure/types/manage/%menu']['page callback'] = '_teams_menu_menu_link_edit_access';
  $items['admin/structure/types/manage/%menu']['page arguments'] = [4];
  $items['admin/structure/menu/manage/%menu/edit']['access callback'] = '_teams_menu_menu_link_edit_access';
  $items['admin/structure/menu/manage/%menu/edit']['access arguments'] = [4];
  $items['admin/structure/menu/item/%menu_link/edit']['access callback'] = '_teams_menu_menu_link_edit_access';
  $items['admin/structure/menu/item/%menu_link/edit']['access arguments'] = [4];
  $items['admin/structure/menu/item/%menu_link/delete']['access callback'] = '_teams_menu_menu_link_edit_access';
  $items['admin/structure/menu/item/%menu_link/delete']['access arguments'] = [4];
}

/**
 * Menu alter access callback.
 *
 * @param array $menu_link The menu link
 *
 * @return bool
 */
function _teams_menu_menu_link_edit_access($menu_link) {
  $menu_edit_access = &drupal_static(__FUNCTION__, []);

  if (isset($menu_link['menu_name'])) {
    $menu_id = $menu_link['menu_name'];
  }
  elseif (isset($menu_link['mlid'])) {
    $menu_id = $menu_link['mlid'];
  }

  if (isset($menu_id)) {
    if (!isset($menu_edit_access[$menu_id])) {
      $menu_edit_access[$menu_id] = user_access('access administration menu') && _teams_menu_has_access($menu_link);
    }

    return $menu_edit_access[$menu_id];
  }

  return user_access('access administration menu');
}

/**
 * @param array $entity The menu entity
 *
 * @return bool
 */
function _teams_menu_has_access($entity) {
  if (!user_is_logged_in()) {
    return FALSE;
  }

  if (teams_user_is_superadmin()) {
    return TRUE;
  }

  if (isset($entity)) {
    if (isset($entity['mlid'])) {
      $menu_teams_access = _teams_menu_find_teams_by_link_id($entity['mlid']);
    }
    elseif (isset($entity['menu_name'])) {
      $menu_teams_access = _teams_menu_find_teams_by_menu_name($entity['menu_name']);
    }
    if (!empty($menu_teams_access)) {
      return teams_has_permission($menu_teams_access);
    }
  }

  return FALSE;
}

/**
 * Find the menu link node by it's id
 *
 * @param integer $mlid The menu link id
 * @param integer $fetch_mode The PDO::FETCH_* mode
 *
 * @return mixed
 */
function _teams_menu_find_teams_by_link_id($mlid, $fetch_mode = NULL) {
  return db_select('teams_menu_links', 'tml')
    ->fields('tml', ['team_id', 'mlid'])
    ->condition('tml.mlid', $mlid, '=')
    ->execute()
    ->fetchAll($fetch_mode);
}

/**
 * Find the menu node by it's name
 *
 * @param integer $name The menu name
 * @param integer $fetch_mode A PDO::FETCH_* mode
 *
 * @return array
 */
function _teams_menu_find_teams_by_menu_name($name, $fetch_mode = NULL) {
  return db_select('teams_menu_custom', 'tmc')
    ->fields('tmc', ['team_id', 'menu_name'])
    ->condition('tmc.menu_name', $name, '=')
    ->execute()
    ->fetchAll($fetch_mode);
}

/**
 * Find the menu names for a list of teams
 *
 * @param array $teams The list of teams
 *
 * @return array
 */
function _teams_menu_find_menu_names_by_teams($teams) {
  $query = db_select('teams_menu_custom', 'tmc');
  $query->join('menu_custom', 'mc', 'tmc.menu_name = mc.menu_name');
  return $query->fields('mc', ['menu_name'])
               ->condition('tmc.team_id', $teams, 'IN')
               ->execute()
               ->fetchAll(PDO::FETCH_COLUMN);
}

/**
 * Find the menu names for a list of teams
 *
 * @param array $teams The list of teams
 *
 * @return array
 */
function _teams_menu_find_menu_titles_by_teams($teams) {
  $query = db_select('teams_menu_custom', 'tmc');
  $query->join('menu_custom', 'mc', 'tmc.menu_name = mc.menu_name');
  return $query->fields('mc', ['title'])
               ->condition('tmc.team_id', $teams, 'IN')
               ->execute()
               ->fetchAll(PDO::FETCH_COLUMN);
}

/**
 * Find the menu links for a list of teams
 *
 * @param array $teams The list of teams
 *
 * @return array
 */
function _teams_menu_find_menu_links_by_teams($teams) {
  return db_select('teams_menu_links', 'tml')
    ->fields('tml', ['mlid', 'team_id'])
    ->condition('tml.team_id', $teams, 'IN')
    ->execute()
    ->fetchAll(PDO::FETCH_COLUMN, 0);
}
