<?php

/**
 * @file
 *
 * Teams taxonomy access module
 */

module_load_include('inc', 'teams_taxonomy', 'includes/admin');
if (module_exists('admin_menu')) {
  module_load_include('inc', 'teams_taxonomy', 'includes/admin_menu');
}

/**
 * Implements hook_menu_alter().
 *
 * Pass menu item through access controller.
 *
 * @param $items
 */
function teams_taxonomy_menu_alter(&$items) {
  $items['taxonomy/term/%taxonomy_term/edit']['page callback'] = '_teams_taxonomy_page_callback';
  $items['taxonomy/term/%taxonomy_term/edit']['page arguments'] = [2];
  $items['admin/structure/taxonomy/%taxonomy_vocabulary_machine_name/edit']['page callback'] = '_teams_taxonomy_page_callback';
  $items['admin/structure/taxonomy/%taxonomy_vocabulary_machine_name/edit']['page arguments'] = [3];
}

/**
 * Page callback for taxonomy term edit, and vocabulary edit
 *
 * @param object $taxonomy The taxonomy
 * @param array $account The user account
 *
 * @return bool
 */
function _teams_taxonomy_page_callback($taxonomy = NULL, $account = NULL) {
  $has_access = FALSE;
  if (user_is_logged_in() && is_object($taxonomy)) {
    if (isset($taxonomy->tid)) {
      $taxonomy_teams = _teams_taxonomy_find_term_data_by_tid($taxonomy->tid);
      $taxonomy_term = taxonomy_term_load($taxonomy->tid);
    }
    elseif (isset($taxonomy->vid)) {
      $taxonomy_teams = _teams_taxonomy_find_vocabulary_by_vid($taxonomy->vid);
      $taxonomy_vocabulary = taxonomy_vocabulary_load($taxonomy->vid);
    }

    if (teams_user_is_superadmin()) {
      $has_access = TRUE;
    }
    elseif (!empty($taxonomy_teams)) {
      $has_access = teams_has_permission($taxonomy_teams);
    }

    $is_taxonomy_term_edit_page = (isset($taxonomy->tid, $taxonomy_term) && current_path() === "taxonomy/term/$taxonomy->tid/edit");
    $is_taxonomy_vocabulary_edit_page = (isset($taxonomy_vocabulary) && current_path() === "admin/structure/taxonomy/$taxonomy_vocabulary->machine_name/edit");
    if (!$has_access && ($is_taxonomy_term_edit_page || $is_taxonomy_vocabulary_edit_page)) {
      _teams_access_denied();
      return;
    }
    elseif ($has_access && $is_taxonomy_term_edit_page) {
      return drupal_get_form('taxonomy_form_term', $taxonomy_term);
    }
    elseif ($has_access && $is_taxonomy_vocabulary_edit_page) {
      return drupal_get_form('taxonomy_form_vocabulary', $taxonomy_vocabulary);
    }
  }

  return $has_access;
}

/**
 * Get the teams that have access rights to this node
 *
 * @param integer $tid The taxonomy id
 * @param boolean $fetch_mode The PDO fetch mode. Optional
 *
 * @return array
 */
function _teams_taxonomy_find_term_data_by_tid($tid, $fetch_mode = NULL) {
  return db_select('teams_term_data', 'ttd')
    ->fields('ttd', ['team_id', 'tid'])
    ->condition('ttd.tid', $tid, '=')
    ->execute()
    ->fetchAll($fetch_mode);
}

/**
 * Get the teams that have access rights to this node
 *
 * @param integer $vid The taxonomy vid
 * @param boolean $fetch_mode The PDO fetch mode. Optional
 *
 * @return array
 */
function _teams_taxonomy_find_vocabulary_by_vid($vid, $fetch_mode = NULL) {
  return db_select('teams_vocabulary', 'tv')
    ->fields('tv', ['team_id', 'vid'])
    ->condition('tv.vid', $vid, '=')
    ->execute()
    ->fetchAll($fetch_mode);
}
