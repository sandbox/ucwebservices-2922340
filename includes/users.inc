<?php

/**
 * @file
 *
 * Users include file used to handle the user interactions within the site for the Teams module
 *
 */

/**
 * On user login add the teams a user is associated with to the users logged in session
 * Note: drupal already uses php session_start() on user log in
 */
function teams_user_login(&$edit, $account) {
  global $user;

  $user_teams = db_select('teams_users', 'tou')
    ->fields('tou', ['team_id'])
    ->condition('tou.uid', $user->uid, '=')
    ->execute()
    ->fetchAll();
  if (empty($user_teams)) {
    $teams_access = NULL;
  }
  else {
    // Add all teams that the user has access to, including children
    $teams_access = [];
    foreach ($user_teams as $team) {
      $team_children = teams_get_teams($team->team_id);
      foreach ($team_children as $child) {
        $teams_access[] = $child['team_id'];
      }
    }
    $teams_access = array_unique($teams_access);
  }

  teams_user_assign_teams($teams_access);
}

/**
 * On user delete remove all user team associations
 * Implements hook_user_delete()
 */
function teams_user_delete($account) {
  // Delete user from teams_users
  db_delete('teams_users')
    ->condition('uid', $account->uid)
    ->execute();
}

/**
 * Implements hook_user_view().
 *
 * Show a list of all teams a user belongs to on their user profile page
 */
function teams_user_view($account) {
  if (!isset($account->content['teams'])) {
    $account->content['teams'] = [];
  }
  $account->content['teams'] += [
    '#type' => 'user_profile_category',
    '#attributes' => ['class' => ['user-member']],
    '#weight' => 5,
    '#title' => t('Teams'),
  ];

  $team_membership_html = NULL;
  $user_teams = teams_get_user_teams();
  if (empty($user_teams)) {
    $team_membership_html .= '&bull; None';
  }
  else {
    $all_teams = teams_find_all();
    foreach ($all_teams as $team) {
      if (in_array($team['team_id'], $user_teams)) {
        $team_membership_html .= '&bull; ' . $team['name'] . '<br>';
      }
    }
  }

  $account->content['teams']['membership'] = [
    '#type' => 'user_profile_item',
    '#title' => t('Team Membership'),
    '#markup' => $team_membership_html,
  ];
}
