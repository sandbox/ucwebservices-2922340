<?php
/**
 * @file
 *
 * Access control alters for admin_menu module.
 */

/**
 * Implements hook_admin_menu_output_alter().
 *
 * Remove links to taxonomy vocabularies that Teams has denied from admin_menu.
 *
 * @param $content
 */
function teams_taxonomy_admin_menu_output_alter(&$content) {
  if (teams_user_is_superadmin()) {
    return;
  }

  $vocs = [];
  foreach (taxonomy_get_vocabularies() as $vocabulary) {
    $vocabulary_id = _teams_taxonomy_find_vocabulary_by_vid($vocabulary->vid);
    foreach ($vocabulary_id as $vid) {
      $voc_names[] = taxonomy_vocabulary_load($vid->vid);
      foreach ($voc_names as $voc_name) {
        $vocs[] = $voc_name->machine_name;
      }
    }
  }

  if (isset($content['menu']['admin/structure']['admin/structure/taxonomy'])) {
    $rows = [];
    foreach ($content['menu']['admin/structure']['admin/structure/taxonomy'] as $row => $taxonomy_menu) {
      if (isset($taxonomy_menu['#title']) && strpos($row, '/add') === FALSE) {
        $split_type = explode("/", trim($row));
        $rows[$row] = $split_type[3];
      }
    }

    foreach ($rows as $r => $row) {
      if (!in_array($row, $vocs)) {
        unset($content['menu']['admin/structure']['admin/structure/taxonomy'][$r]);
      }
    }
  }
}
