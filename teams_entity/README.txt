Description
-----------
Teams Entity adds Teams integration for Drupal entities.
This will allow an administrator to assign ownership of entities to a particular team (or teams).

If you have not added a team already, please review the steps in the core teams README.txt file.

Usage
-----
1. Add / edit any entities.

2. Add ownership for an entity to a Team (or teams) under the Teams vertical tab.

3. Click "Save"

4. Members of the team(s) that were assigned ownership should now have access to edit/delete.

If a user is not a member of this team they will not have edit/delete permission once ownership
has been assigned.

Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/teams