Description
-----------
Teams IMCE adds Teams integration for the IMCE module (http://drupal.org/project/imce).
This will allow an administrator to assign ownership of files to a particular team (or teams).

If you have not added a team already, please review the steps in the core teams README.txt file.

Installation
------------
1. Module will add folders for each existing team during installation.

2. A Teams IMCE profile will be added during installation.

3. To use the Teams profile, assign it to a role under /admin/config/media/imce.

Usage
-----
1. Open IMCE

2. Only folders for teams that you are assigned to will display in the left panel.

3. Upload files as usual.

Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/teams