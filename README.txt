Description
-----------
Teams is part of a suite of modules that allows users to be grouped together into Teams.
Teams can then be granted access to edit/delete content, users that are not part of the
team that has ownership of the content cannot edit/delete this content.

Core functionality
------------------
This module includes the core functionality of the Teams module suite.
Access control checks for team membership are validated through this module.

This module also includes administrative views to add/remove teams and to
add and remove users from teams.

Submodules
----------
Teams includes a number of submodules for specific functionality and integration
with third party modules such as Fieldable Panels Panes, IMCE, Page Manager, and Views.

Please view the README.txt files for each submodule for specific functionality provided.

Installation
------------
1. Copy the entire teams modules to Drupal's sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Add a user to a team under /admin/config/teams

4. Enable any required submodules required for your site (e.g. Nodes, Node Types, etc.)

Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/teams