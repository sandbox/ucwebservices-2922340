<?php

require_once __DIR__ . '/helpers.test';

/**
 * Teams Functional Tests
 */
class TeamsFunctionalTestCase extends DrupalWebTestCase {

  /**
   * -------------------------------------------------------------------------
   * Test Setup
   * -------------------------------------------------------------------------
   */

  /**
   * Setup the module, including database fixtures
   */
  public function setUp() {
    parent::setUp('teams');
  }

  /**
   * Provide information on the SimpleTest page
   *
   * @return array
   */
  public static function getInfo() {
    return [
      'name' => 'Teams Functional Tests',
      'description' => 'Functional tests for Teams',
      'group' => 'Teams',
    ];
  }

  /**
   * -------------------------------------------------------------------------
   * Tests
   * -------------------------------------------------------------------------
   */

  /**
   * @see teams_get_teams()
   */
  public function test_teams_get_teams() {
    _teams_populate(_teams_fixture());

    // Get an invalid team
    $this->assertEqual(teams_get_teams(PHP_INT_MAX), [], 'User has no teams');

    // Get a valid team with one child
    $teams = [
      4 => [
        "name" => "Department Dolphin",
        "team_id" => "4",
        "parent_id" => "1",
        "status" => "Active",
      ],
      5 => [
        "name" => "Centre for Elephant",
        "team_id" => "5",
        "parent_id" => "4",
        "status" => "Active",
      ],
    ];
    $this->assertEqual(teams_get_teams(4), $teams, 'User has correct teams');
  }
}
