<?php
/**
 * @file
 *
 * Admin file for Teams Nodes module
 */

/**
 * Implements hook_form_alter().
 */
function teams_nodes_form_node_form_alter(&$form, &$form_state, $form_id) {
  if (!user_is_logged_in()) {
    return;
  }

  // Get list of all teams
  $all_teams = [];
  foreach (teams_get_teams() as $team) {
    $all_teams[$team['team_id']] = $team['name'];
  }

  $is_add_page = !isset($form['nid']['#value']);
  if ($is_add_page ) {

    $module_id = teams_add_form_elements($form, [], 'nodes');

    // Disable all teams that you are not a part of and should not be able to select
    $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
    foreach ($disabled_teams as $team_id) {
      $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
    }

    // On add node form submit create handler
    // Note: using $form['actions']['submit']['#submit'][] will give you the nid on submit
    // The standard $form['#submit'] will not return nid on submit
    $form['actions']['submit']['#submit'][] = '_teams_nodes_form_submit_handler';
  }
  else {
    $default_checked_teams =_teams_nodes_find_nodes_by_nid($form['nid']['#value'], PDO::FETCH_COLUMN);

    // Attach teams display to form as content
    $module_id = teams_add_form_elements($form, $default_checked_teams, 'nodes');

    // Only allow the admin user to update teams after node creation
    $is_admin_user = teams_user_is_superadmin();
    if (!$is_admin_user) {
      foreach ($all_teams as $team_id => $team_name) {
        $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
      }
    } else {
      $form['actions']['submit']['#submit'][] = '_teams_nodes_form_submit_handler';

      // Disable all teams that you are not a part of and should not be able to select
      $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
      foreach ($disabled_teams as $team_id) {
        $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;

        // Add a hidden input for this checkbox so we still get the form value on submit, but it isn't changeable
        if (in_array($team_id, $default_checked_teams)) {
          $form[$module_id]['teams[' . $team_id . ']'] = [
            '#type' => 'hidden',
            '#value' => $team_id,
          ];
        }
      }
    }
  }
}

/**
 * On node form submit
 */
function _teams_nodes_form_submit_handler(&$form, &$form_state) {
  $node_id = $form_state['node']->nid;
  $current_team_ids = _teams_nodes_find_nodes_by_nid($node_id, PDO::FETCH_COLUMN);

  $submitted_team_ids = [];
  foreach ($form_state['values']['teams'] as $team_id) {
    if ($team_id !== 0) {
      $submitted_team_ids[] = $team_id;
    }
  }

  $removed_team_ids = array_diff($current_team_ids, $submitted_team_ids);
  if (!empty($removed_team_ids)) {
    $result = db_delete('teams_nodes')
      ->condition('team_id', $removed_team_ids, 'IN')
      ->condition('nid', $node_id, '=')
      ->execute();

    if ($result === NULL) {
      drupal_set_message(t("Error removing teams from node"), 'error');
    }
  }

  $added_team_ids = array_diff($submitted_team_ids, $current_team_ids);
  if (!empty($added_team_ids)) {
    $query = db_insert('teams_nodes')->fields(['nid', 'team_id', 'modified', 'created']);
    foreach ($added_team_ids as $team_id) {
      $query->values([
                       $node_id,
                       $team_id,
                       REQUEST_TIME,
                       REQUEST_TIME,
                     ]);
    }

    if ($query->execute() === NULL) {
      drupal_set_message(t("Error updating node team membership"), 'error');
    }
  }

  drupal_set_message(t("Node team membership updated"));
}

/**
 * On node delete clean up custom database tables
 */
function teams_nodes_delete($node) {
  // Remove all references to deleted node in teams_nodes table
  db_delete('teams_nodes')
    ->condition('nid', $node->nid)
    ->execute();
}
