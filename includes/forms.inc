<?php

/**
 * @file
 *
 * Admin include file used to handle interfacing with the form api
 *
 */

/**
 * Add team form callback function
 * Used to create the add team form page
 *
 * @return array
 */
function add_edit_team_form($form, &$form_state) {
  $team_id = arg(5);

  // Define form array
  $form['add_edit_team_form'] = [];

  $form['add_edit_team_form']['form_type'] = [
    '#type' => 'hidden',
    '#value' => 'add',
    '#required' => TRUE,
  ];
  $form['add_edit_team_form']['name'] = [
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
  ];
  $form['add_edit_team_form']['description'] = [
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#rows' => 5,
    '#required' => FALSE,
  ];

  $all_teams_list = teams_order_teams(teams_find_all(), $parent_id = 0, '- ');
  $all_teams = [];
  $all_teams[0] = 'None';
  $ignored_team_parents = [];
  foreach ($all_teams_list as $team) {
    // Skip the team_id we're currently editing
    if ($team['team_id'] == $team_id) {
      $ignored_team_parents[] = $team['team_id'];
      continue;
    }

    // Skip any teams that are a child of the team we're currently editing
    if (in_array($team['parent_id'], $ignored_team_parents)) {
      $ignored_team_parents[] = $team['team_id'];
      continue;
    }

    $all_teams[$team['team_id']] = $team['name'];
  }
  $form['add_edit_team_form']['parent_id'] = [
    '#type' => 'select',
    '#title' => t('Parent'),
    '#required' => TRUE,
    '#options' => $all_teams,
  ];

  $form['add_edit_team_form']['status'] = [
    '#type' => 'select',
    '#default_value' => 'Active',
    '#title' => t('Status'),
    '#options' => ['Active' => 'Active', 'Closed' => 'Closed'],
    '#required' => TRUE,
  ];
  $form['add_edit_team_form']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Add'),
  ];

  // Check for edit mode (If in edit mode populate default field values)
  if (isset($form_state['build_info']['args'][0]) && $form_state['build_info']['args'][0] === 'edit form') {
    // Get the team name based on the team_id passed through the url
    $team_name = db_select('teams_teams', 'tot')
      ->fields('tot', ['team_id', 'name', 'description', 'parent_id', 'status'])
      ->condition('tot.team_id', $team_id, '=')
      ->execute()
      ->fetchAssoc();

    // Add edit values to form based off selected team for update
    $form['add_edit_team_form']['team_id'] = [
      '#type' => 'hidden',
      '#value' => $team_name['team_id'],
      '#required' => TRUE,
    ];
    $form['add_edit_team_form']['form_type']['#value'] = 'edit';
    $form['add_edit_team_form']['name']['#default_value'] = $team_name['name'];
    $form['add_edit_team_form']['description']['#default_value'] = $team_name['description'];
    $form['add_edit_team_form']['parent_id']['#default_value'] = $team_name['parent_id'];
    $form['add_edit_team_form']['status']['#default_value'] = $team_name['status'];
    $form['add_edit_team_form']['submit']['#value'] = 'Update';
  }

  return $form;
}

/**
 * On add team form submit, add new team to db using db_insert
 */
function add_edit_team_form_submit($form, $form_state) {
  if (isset($form_state['values']['form_type']) && $form_state['values']['form_type'] === 'add') {
    // Insert new team into db table for teams
    try{
      $result = db_insert('teams_teams')
      ->fields([
                 'name' => $form_state['values']['name'],
                 'description' => $form_state['values']['description'],
                 'parent_id' => $form_state['values']['parent_id'],
                 'status' => $form_state['values']['status'],
                 'modified' => REQUEST_TIME,
                 'created' => REQUEST_TIME,
               ])
      ->execute();
      if ($result === NULL) {
        drupal_set_message(t("Error adding team"), 'error');
      } else {
        drupal_set_message(t('Team added'));

        // In order to prevent the user from having to logout and login, if the new team is a child of
        // a team that this user has access to, add the new team to the user's current list of teams
        $user_teams = teams_get_user_teams();
        if ($form_state['values']['parent_id'] > 0 && in_array($form_state['values']['parent_id'], $user_teams)) {
          $user_teams[] = $result;
          teams_user_assign_teams($user_teams);
        }
      }
    }
    catch(Exception $e){
      drupal_set_message(t('Error adding new team, Team name may already exist'), 'error');
    }
  }
  elseif (isset($form_state['values']['form_type']) && $form_state['values']['form_type'] === 'edit') {
    // Insert new team into db table for teams
    $result = db_update('teams_teams')
      ->fields([
                 'name' => $form_state['values']['name'],
                 'description' => $form_state['values']['description'],
                 'parent_id' => $form_state['values']['parent_id'],
                 'status' => $form_state['values']['status'],
                 'modified' => REQUEST_TIME,
               ])
      ->condition('team_id', $form_state['values']['team_id'], '=')
      ->execute();

    // Check if edit team was successful
    if ($result === NULL) {
      drupal_set_message(t('Error adding/updating team'), 'error');
    }
    else {
      drupal_set_message(t($form_state['values']['name']));
    }
  }
  else {
    // Alert user of error
    drupal_set_message(t('Error adding/updating team'), 'error');
  }
}

/**
 * Remove team form callback function
 * Used to remove teams from db
 *
 * @return array
 */
function remove_team_form($form, &$form_state) {
  // Get the team name based on the team_id passed through the url
  $team_name = db_select('teams_teams', 'tot')
    ->fields('tot', ['team_id', 'name'])
    ->condition('tot.team_id', arg(5), '=')
    ->execute()
    ->fetchAssoc();

  return [
    'message' => [
      '#markup' => "
    <div>
      <h1>Are you sure you want to remove: $team_name[name]</h1>
      <br>
    </div>",
    ],
    'team_id' => [
      '#type' => 'hidden',
      '#value' => $team_name['team_id'],
    ],
    'remove' => [
      '#type' => 'submit',
      '#value' => 'Remove',
    ],
    'cancel' => [
      '#markup' => '<a href="/admin/config/teams">Cancel</a>',
    ],
  ];
}

/**
 * On remove team form submit, remove team and all its associated users from db
 */
function remove_team_form_submit($form, $form_state) {
  // Delete user from teams_users
  $teams_users_delete = db_delete('teams_users')
    ->condition('team_id', $form_state['values']['team_id'])
    ->execute();

  // Delete team from teams_teams
  $teams_teams_delete = db_delete('teams_teams')
    ->condition('team_id', $form_state['values']['team_id'])
    ->execute();

  // Check for errors before delete
  if (empty($teams_users_delete) && empty($teams_teams_delete)) {
    drupal_set_message(t('Error removing team'), 'error');
  }
  else {
    drupal_set_message(t('Team removed'));
  }
}

/**
 * add user to team callback function
 * Form used to add a user to a team
 *
 * @return array
 */
function assign_team_user_form($form, &$form_state) {
  $user_id = arg(6);
  $username = db_select('users', 'u')
    ->fields('u', ['name'])
    ->condition('u.uid', $user_id, '=')
    ->execute()
    ->fetchField();

  // Get the list of teams the selected user belongs to
  $user_teams = db_select('teams_users', 'tu')
    ->fields('tu', ['team_id'])
    ->condition('tu.uid', $user_id, '=')
    ->execute()
    ->fetchAll(PDO::FETCH_COLUMN);

  $all_teams = NULL;
  foreach (teams_get_teams() as $team) {
    $all_teams[$team['team_id']] = $team['name'];
  }

  $form = [
    'message' => [
      '#markup' => "
    <div>
      <h1>Please select a team to add: $username</h1>
      <br>
    </div>",
    ],
    'teams' => [
      '#type' => 'checkboxes',
      '#title' => t('Teams'),
      '#default_value' => $user_teams,
      '#options' => $all_teams,
    ],
    'uid' => [
      '#type' => 'hidden',
      '#value' => $user_id,
    ],
    'add' => [
      '#type' => 'submit',
      '#value' => 'Add',
    ],
    'cancel' => [
      '#markup' => '<a href="/admin/config/teams/assign">Cancel</a>',
    ],
  ];

  // Disable all teams that you are not a part of, and should not be able to select
  $is_teams_admin = teams_user_is_superadmin();
  $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
  foreach ($disabled_teams as $team_id) {
    if (!$is_teams_admin) {
      $form['teams'][$team_id]['#disabled'] = TRUE;

      // Add a hidden input for this checkbox so we still get the form value on submit, but it isn't changeable
      if (in_array($team_id, $user_teams)) {
        $form['teams[' . $team_id . ']'] = [
          '#type' => 'hidden',
          '#value' => $team_id,
        ];
      }
    }
  }

  return $form;
}

/**
 * On team user add form submit
 */
function assign_team_user_form_submit($form, $form_state) {
  $user_id = $form_state['values']['uid'];
  $current_team_ids = db_select('teams_users', 'tu')
    ->fields('tu', ['team_id'])
    ->condition('tu.uid', $user_id, '=')
    ->execute()
    ->fetchAll(PDO::FETCH_COLUMN);

  $submitted_team_ids = [];
  foreach ($form_state['values']['teams'] as $team_id) {
    if ($team_id !== 0) {
      $submitted_team_ids[] = $team_id;
    }
  }

  $removed_team_ids = array_diff($current_team_ids, $submitted_team_ids);
  if (!empty($removed_team_ids)) {
    $result = db_delete('teams_users')
      ->condition('team_id', $removed_team_ids, 'IN')
      ->condition('uid', $user_id, '=')
      ->execute();

    if ($result === NULL) {
      drupal_set_message(t("Error removing teams from user"), 'error');
    }
  }

  $added_team_ids = array_diff($submitted_team_ids, $current_team_ids);
  if (!empty($added_team_ids)) {
    $query = db_insert('teams_users')->fields(['uid', 'team_id', 'modified', 'created']);
    foreach ($added_team_ids as $team_id) {
      $query->values([$user_id, $team_id, REQUEST_TIME, REQUEST_TIME]);
    }
    if ($query->execute() === NULL) {
      drupal_set_message(t("Error updating user\'s teams membership"), 'error');
    }
  }

  // if current user is being edited, update their session
  global $user;
  if ($user->uid === $user_id) {
    teams_user_assign_teams($submitted_team_ids);
  }
  drupal_set_message(t("User's team membership updated"));
}

/**
 * Remove user from team callback function
 * Used to remove a user from being associated with a specific team within the db
 *
 * @return array
 */
function remove_team_user_form($form, &$form_state) {
  $team_id = arg(6);
  $user_id = arg(7);

  if (!in_array($team_id, teams_get_user_teams())) {
    _teams_access_denied();
    return;
  }

  $team_name = db_select('teams_teams', 'tot')
    ->fields('tot', ['name'])
    ->condition('tot.team_id', $team_id, '=')
    ->execute()
    ->fetchField();

  $team_user_query = db_select('teams_users', 'tou');
  $team_user_query->join('users', 'u', 'u.uid = tou.uid');
  $user_name = $team_user_query->fields('u', ['name'])
        ->condition('tou.team_id', $team_id, '=')
        ->condition('tou.uid', $user_id, '=')
        ->execute()
        ->fetchField();

  return [
    'message' => [
      '#markup' => "
    <div>
      <h1>Are you sure you want to remove: $user_name from $team_name?</h1>
      <br>
    </div>",
    ],
    'team_id' => [
      '#type' => 'hidden',
      '#value' => $team_id,
    ],
    'uid' => [
      '#type' => 'hidden',
      '#value' => $user_id,
    ],
    'remove' => [
      '#type' => 'submit',
      '#value' => 'Remove',
    ],
    'cancel' => [
      '#markup' => '<a href="/admin/config/teams/team/users/' . $team_id . '">Cancel</a>',
    ],
  ];
}

/**
 * On remove team form submit, remove team and all its associated users from db
 */
function remove_team_user_form_submit($form, $form_state) {
  // Delete user from teams_users for specific team
  $result = db_delete('teams_users')
    ->condition('team_id', $form_state['values']['team_id'])
    ->condition('uid', $form_state['values']['uid'])
    ->execute();

  // Check user removed from team success
  if ($result === NULL) {
    drupal_set_message(t('Error removing user from team'), 'error');
  }
  else {
    drupal_set_message(t('User removed from team'));
  }
}

/**
 * Teams form alters used to implement Teams options throughout the drupal admin interface
 */
function teams_form_alter(&$form, &$form_state, $form_id) {
  // Add Teams team select options to the user profiles form
  if ($form_id === 'user_profile_form') {

    // Get list of all teams
    $all_teams = [];
    foreach (teams_get_teams() as $team) {
      $all_teams[$team['team_id']] = $team['name'];
    }

    $user_id = $form['#user']->uid;
    $user_teams = teams_get_teams_by_uid($user_id);

    // Add teams display to form
    $form['teams'] = [
      '#type' => 'checkboxes',
      '#title' => t('Teams'),
      '#default_value' => $user_teams,
      '#disabled' => TRUE,
      '#options' => $all_teams,
      '#submit' => 'user_profile_form_submit_handler',
    ];
  }

  // Redirect handler setup for remove_team_form on submit
  elseif ($form_id === 'add_edit_team_form') {
    $form['#submit'][] = 'add_team_form_submit_handler';
  }

  // Redirect handler setup for remove_team_form on submit
  elseif ($form_id === 'remove_team_form') {
    $form['#submit'][] = 'remove_team_form_submit_handler';
  }

  // Redirect handler setup for assign_team_user_form on submit
  elseif ($form_id === 'assign_team_user_form') {
    $form['#submit'][] = 'assign_team_user_form_submit_handler';
  }

  // Redirect handler setup for remove_team_form on submit
  elseif ($form_id === 'remove_team_user_form') {
    $form['#submit'][] = 'remove_team_user_form_submit_handler';
  }
}

/**
 * On form submit for user_profile_form
 */
function user_profile_form_submit_handler(&$form, &$form_state) {
  // On submit update user team associations here
}

/**
 * Redirect for add_team_form
 */
function add_team_form_submit_handler(&$form, &$form_state) {
  $form_state['redirect'] = '/admin/config/teams';
}

/**
 * Redirect for remove_team_form
 */
function remove_team_form_submit_handler(&$form, &$form_state) {
  $form_state['redirect'] = '/admin/config/teams';
}

/**
 * Redirect for remove_team_form
 */
function assign_team_user_form_submit_handler(&$form, &$form_state) {
  $form_state['redirect'] = '/admin/config/teams/assign';
}

/**
 * Redirect for remove_team_user_form
 */
function remove_team_user_form_submit_handler(&$form, &$form_state) {
  $form_state['redirect'] = '/admin/config/teams/team/users/' . $form_state['values']['team_id'];
}
