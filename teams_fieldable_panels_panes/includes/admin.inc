<?php

/**
 * @file
 *
 * Admin file for Teams Fieldable Panels Panes module
 */

/**
 * Implements hook_form_alter().
 *
 * Teams Fieldable Panels Panes form alter used to integrate ownership into admin display
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function teams_fieldable_panels_panes_form_alter(&$form, &$form_state, $form_id) {
  if (!user_is_logged_in()) {
    return;
  }

  // Make sure this is an fieldable panels pane form
  $allowed_form_ids = [
    // Edit an FPP on Structure => FPP's
    'fieldable_panels_panes_entity_edit_form',

    // Add an FPP to a page on Pages => Edit Content
    'fieldable_panels_panes_fieldable_panels_pane_content_type_edit_form',
  ];
  if (!isset($form['#entity_type']) || $form['#entity_type'] !== 'fieldable_panels_pane' || !in_array($form_id, $allowed_form_ids)) {
    return;
  }

  // Create array to hold team names
  $all_teams = [];
  foreach (teams_get_teams() as $team) {
    $all_teams[$team['team_id']] = $team['name'];
  }

  $is_add_fpp = !isset($form['#entity']->fpid);
  if ($is_add_fpp) {

    $module_id = teams_add_form_elements($form, '', 'fieldable_panels_panes');

    // Disable all teams that you are not a part of and should not be able to select
    $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
    foreach ($disabled_teams as $team_id) {
      $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
    }

    // On add entity form submit create handler
    // Note: using $form['actions']['submit']['#submit'][] will not give you an id on submit
    // The standard $form['#submit'] will return an id on submit
    $form['#submit'][] = '_teams_fieldable_panels_panes_form_submit_handler';
  }
  else {
    // Get the teams that have access rights to this fieldable panels pane
    $default_checked_teams = _teams_fieldable_panels_panes_find_teams_by_fpid($form['#entity']->fpid, PDO::FETCH_COLUMN);

    $module_id = teams_add_form_elements($form, $default_checked_teams, 'fieldable_panels_panes');

    // Only allow the admin user to update teams after node creation
    $is_admin_user = teams_user_is_superadmin();
    if (!$is_admin_user) {
      foreach ($all_teams as $team_id => $team_name) {
        $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
      }
    }
    else {
      $form['#submit'][] = '_teams_fieldable_panels_panes_form_submit_handler';

      // Disable all teams that you are not a part of and should not be able to select
      $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
      foreach ($disabled_teams as $team_id) {
        $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;

        // Add a hidden input for this checkbox so we still get the form value on submit, but it isn't changeable
        if (in_array($team_id, $default_checked_teams)) {
          $form[$module_id]['teams[' . $team_id . ']'] = [
            '#type' => 'hidden',
            '#value' => $team_id,
          ];
        }
      }
    }
  }
}

/**
 * Fieldable Panels Panes form submit
 *
 * @param $form
 * @param $form_state
 */
function _teams_fieldable_panels_panes_form_submit_handler(&$form, &$form_state) {
  $fpp_id = $form_state['entity']->fpid;
  $current_team_ids = _teams_fieldable_panels_panes_find_teams_by_fpid($fpp_id, PDO::FETCH_COLUMN);

  $submitted_team_ids = [];
  foreach ($form_state['values']['teams'] as $team_id) {
    if ($team_id !== 0) {
      $submitted_team_ids[] = $team_id;
    }
  }

  $removed_team_ids = array_diff($current_team_ids, $submitted_team_ids);
  if (!empty($removed_team_ids)) {
    $result = db_delete('teams_fieldable_panels_panes')
      ->condition('team_id', $removed_team_ids, 'IN')
      ->condition('fpid', $fpp_id, '=')
      ->execute();

    if ($result === NULL) {
      drupal_set_message(t("Error removing teams from FPP"), 'error');
    }
  }

  $added_team_ids = array_diff($submitted_team_ids, $current_team_ids);
  if (!empty($added_team_ids)) {
    $query = db_insert('teams_fieldable_panels_panes')->fields(['fpid', 'team_id', 'modified', 'created']);
    foreach ($added_team_ids as $team_id) {
      $query->values([
                       $fpp_id,
                       $team_id,
                       REQUEST_TIME,
                       REQUEST_TIME,
                     ]);
    }

    if ($query->execute() === NULL) {
      drupal_set_message(t("Error updating FPP team membership"), 'error');
    }
  }

  drupal_set_message(t("FPP team membership updated"));
}

/**
 * Implements hook_fieldable_panels_pane_delete().
 *
 * Cleanup Fieldable Panels Panes in Teams table when deleted
 *
 * @param $panels_pane
 */
function teams_fieldable_panels_panes_fieldable_panels_pane_delete($panels_pane) {
  // Remove all references to deleted entity in teams_entity table
  db_delete('teams_fieldable_panels_panes')
    ->condition('fpid', $panels_pane->fpid)
    ->execute();
}

/**
 * Implements hook_contextual_links_view_alter().
 *
 * Allow access to edit and delete content via contextual links
 * when user is a member of team
 */
function teams_fieldable_panels_panes_contextual_links_view_alter(&$element, $items) {
  // Only operate on FPP's
  if (!isset($element['#element'], $element['#element']['#entity_type']) || $element['#element']['#entity_type'] !== 'fieldable_panels_pane') {
    return;
  }

  if (teams_user_is_superadmin()) {
    return;
  }

  // Assemble the list of Admin Link FPID's
  $admin_link_fpids = NULL;
  foreach ($element['#element'] as $admin_link) {
    if (isset($admin_link->fpid)) {
      $admin_link_fpids['fpid'] = $admin_link;
    }
  }

  // Run fpid through access function
  if (!empty($admin_link_fpids)) {
    foreach ($admin_link_fpids as $fpid) {
      // @TODO: Should this remove something from the $element array?
      $access = teams_fieldable_panels_panes_access('update', $fpid->fpid);
    }
  }
}
