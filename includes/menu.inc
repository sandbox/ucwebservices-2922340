<?php

/**
 * @file
 *
 * Admin include file used to handle interfacing with the menu api
 */

/**
 * Add Teams items to admin menu
 *
 * @return array
 */
function teams_menu() {
  return [
    'admin/config/teams' => [
      'title' => 'Teams',
      'type' => MENU_NORMAL_ITEM,
      'description' => 'Main Teams admin interface page, used to search and display teams',
    ],
    'admin/config/teams/team/add' => [
      'title' => 'Add team',
      'type' => MENU_NORMAL_ITEM,
      'description' => 'Add new teams form',
      'page callback' => 'drupal_get_form',
      'page arguments' => ['add_edit_team_form', 'add form'],
      'access callback' => 'user_access',
      'access arguments' => [TEAMS_PERMISSION_ADD_TEAM],
    ],
    'admin/config/teams/team/edit' => [
      'title' => 'Edit Team',
      'type' => MENU_SUGGESTED_ITEM,
      'description' => 'Edit new teams form',
      'page callback' => 'drupal_get_form',
      'page arguments' => ['add_edit_team_form', 'edit form'],
      'access callback' => 'user_access',
      'access arguments' => [TEAMS_PERMISSION_EDIT_TEAM],
    ],
    'admin/config/teams/team/remove' => [
      'title' => 'Remove Team',
      'type' => MENU_SUGGESTED_ITEM,
      'description' => 'Remove team form',
      'page callback' => 'drupal_get_form',
      'page arguments' => ['remove_team_form'],
      'access callback' => 'user_access',
      'access arguments' => [TEAMS_PERMISSION_DELETE_TEAM],
    ],
    'admin/config/teams/team/update' => [
      'title' => 'Bulk Update',
      'type' => MENU_SUGGESTED_ITEM,
      'description' => 'A form to update variables.',
      'page callback' => 'drupal_get_form',
      'access callback' => 'user_access',
      'access arguments' => [TEAMS_PERMISSION_BULK_UPDATE],
    ],
    'admin/config/teams/team/users/assign' => [
      'title' => 'Assign user to Team',
      'type' => MENU_SUGGESTED_ITEM,
      'description' => 'Assign user to a selected team.',
      'page callback' => 'drupal_get_form',
      'page arguments' => ['assign_team_user_form'],
      'access callback' => TRUE,
    ],
    'admin/config/teams/team/users/remove' => [
      'title' => 'Remove User From Team',
      'type' => MENU_SUGGESTED_ITEM,
      'description' => 'Remove a user from the selected team.',
      'page callback' => 'drupal_get_form',
      'page arguments' => ['remove_team_user_form'],
      'access callback' => 'user_access',
      'access arguments' => [TEAMS_PERMISSION_REMOVE_USER],
    ],
    'admin/config/teams/settings' => [
      'title' => 'Settings',
      'type' => MENU_SUGGESTED_ITEM,
      'description' => 'Teams configuration settings.',
      'page callback' => 'drupal_get_form',
      'access callback' => 'user_access',
      'access arguments' => [TEAMS_PERMISSION_ADMINISTER],
    ],
  ];
}
