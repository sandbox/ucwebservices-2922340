<?php
/**
 * @file
 *
 * Access control alters for admin_menu module.
 */

/**
 * Implements hook_admin_menu_output_alter().
 *
 * Remove links to content types that Teams has denied from admin_menu.
 *
 * @param $content
 */
function teams_node_type_admin_menu_output_alter(&$content) {
  if (teams_user_is_superadmin()) {
    return;
  }

  $types = [];
  foreach (node_type_get_types() as $node_type => $info) {
    $node_types = _teams_node_type_find_teams_by_type($node_type);
    foreach ($node_types as $type) {
      $types[] = $type->type;
    }
  }

  if (isset($content['menu']['admin/structure']['admin/structure/types'])) {
    $rows = [];
    foreach ($content['menu']['admin/structure']['admin/structure/types'] as $row => $type_menu) {
      if (isset($type_menu['#title']) && strpos($row, '/types/add') === FALSE) {
        $split_type = explode("/", trim($row));
        $rows[$row] = $split_type[4];
      }
    }

    foreach ($rows as $r => $row) {
      if (!in_array($row, $types)) {
        unset($content['menu']['admin/structure']['admin/structure/types'][$r]);
      }
    }
  }
}
