<?php

/**
 * Implements hook_admin_menu_output_alter().
 *
 * Remove nodes from the menu that the user doesn't have access to through their Team membership
 *
 * @param array $content A structured array suitable for drupal_render(). Passed by reference.
 */
function teams_views_admin_menu_output_alter(&$content) {
  if (isset($content['menu']['admin/structure']['admin/structure/views'])) {
    if (teams_user_is_superadmin()) {
      return;
    }

    $menu_items = $content['menu']['admin/structure']['admin/structure/views'];
    foreach ($menu_items as $menu_index => $menu_item) {
      $is_view_menu_item = isset($menu_item['#href']) && substr_count($menu_item['#href'], 'admin/structure/views/view') > 0;
      if ($is_view_menu_item) {
        $view_machine_name = arg(4, $menu_item['#href']);
        $has_permission = _teams_views_has_permission_by_machine_name($view_machine_name, teams_get_user_teams());
        if (!$has_permission) {
          unset($content['menu']['admin/structure']['admin/structure/views'][$menu_index]);
        }
      }
    }
  }
}

/**
 * Can the current user access the specified view?
 *
 * @param string $machine_name The view machine name
 * @param array $user_teams The user teams
 *
 * @return boolean
 */
function _teams_views_has_permission_by_machine_name($machine_name, $user_teams) {
  if (empty($user_teams)) {
    return FALSE;
  }

  $num_results = db_select('teams_views', 'tv')
    ->fields('tv', ['vid'])
    ->condition('tv.machine_name', $machine_name, '=')
    ->condition('tv.team_id', $user_teams, 'IN')
    ->countQuery()
    ->execute()
    ->fetchField();

  return $num_results > 0;
}
