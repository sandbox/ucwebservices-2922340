<?php
/**
 * @file
 *
 * Admin file for Teams pages module
 */

/**
 *
 * Implements hook_form_alter().
 *
 * Page access hook used with Teams api to set page level access for teams
 */
function teams_pages_form_alter(&$form, &$form_state, $form_id) {
  if (!user_is_logged_in()) {
    return;
  }

  // Triggered when the page content is saved
  if ($form_id === 'page_manager_save_page_form') {
    // Get list of all teams
    $all_teams = [];
    foreach (teams_get_teams() as $team) {
      $all_teams[$team['team_id']] = $team['name'];
    }

    $is_add_page = (isset($form_state['page']->new) && $form_state['page']->new === TRUE);
    if ($is_add_page) {
      // Add team select options to node add/edit admin pages
      $module_id = teams_add_form_elements($form, [], 'pages');

      // Disable all teams that you are not a part of, and should not be able to select
      $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
      foreach ($disabled_teams as $team_id) {
        $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
      }

      // On add page form submit handler
      $form['save']['#submit'][] = '_teams_pages_form_submit_handler';
    }
    else {
      // Can the logged in user access this page?
      $page_teams = _teams_pages_find_pages_by_pid($form_state['page']->subtask['subtask']->pid);
      if (!teams_has_permission($page_teams)) {
        _teams_access_denied();
        return;
      }

      // Create and populate array to hold all teams that have access to current node for pre select
      $default_checked_teams = [];
      foreach ($page_teams as $team) {
        $default_checked_teams[] = $team->team_id;
      }

      $module_id = teams_add_form_elements($form, $default_checked_teams, 'pages');

      $url_args = arg();
      $is_edit_content_page = ($url_args[count($url_args) - 1] === 'content');
      if ($is_edit_content_page) {
        // Only allow the admin user to update teams after node creation
        $is_admin_user = teams_user_is_superadmin();
        if (!$is_admin_user) {
          foreach ($all_teams as $team_id => $team_name) {
            $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
          }
        }
        else {
          // Only allow the user to update the teams on the Edit Content page
          $form[$module_id]['save'] = [
            '#type' => 'submit',
            '#value' => 'Save Teams',
            '#submit' => [
              '_teams_pages_form_submit_handler',
            ],
          ];

          // Disable all teams that you are not a part of, and should not be able to select
          $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
          foreach ($disabled_teams as $team_id) {
            $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;

            // Add a hidden input for this checkbox so we still get the form value on submit, but it isn't changeable
            if (in_array($team_id, $default_checked_teams)) {
              $form[$module_id]['teams[' . $team_id . ']'] = [
                '#type' => 'hidden',
                '#value' => $team_id,
              ];
            }
          }
        }
      }
      else {
        // Prevent the user from updating any teams
        foreach ($all_teams as $team_id => $team_name) {
          $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
        }
      }
    }
  }

  // Pages -> Panel Content (display the edit content page)
  elseif ($form_id === 'panels_panel_context_edit_content') {
    $is_edit_page = (isset($form_state['page']->new) && $form_state['page']->new === TRUE && $form_state['type'] === 'edit');
    if ($is_edit_page) {
      // Hide default save button because it skips validation
      $form['buttons']['return']['#type'] = 'hidden';
      $form['buttons']['save']['#type'] = 'hidden';
    }
  }

  // Pages -> ???
  elseif ($form_id === 'page_manager_page_form_delete') {
    $form['#submit'][] = '_teams_pages_delete_submit_handler';
  }
}

/**
 * On page form submit
 */
function _teams_pages_form_submit_handler(&$form, &$form_state) {
  $page_id = $form_state['page']->subtask['subtask']->pid;
  $current_team_ids = _teams_pages_find_pages_by_pid($page_id, PDO::FETCH_COLUMN);

  $submitted_team_ids = [];
  foreach ($form_state['values']['teams'] as $team_id) {
    if ($team_id !== 0) {
      $submitted_team_ids[] = $team_id;
    }
  }

  $removed_team_ids = array_diff($current_team_ids, $submitted_team_ids);
  if (!empty($removed_team_ids)) {
    $result = db_delete('teams_pages')
      ->condition('team_id', $removed_team_ids, 'IN')
      ->condition('pid', $page_id, '=')
      ->execute();

    if ($result === NULL) {
      drupal_set_message(t("Error removing teams from page"), 'error');
    }
  }

  $added_team_ids = array_diff($submitted_team_ids, $current_team_ids);
  if (!empty($added_team_ids)) {
    $page_machine_name = $form_state['page']->subtask['subtask']->name;

    $query = db_insert('teams_pages')->fields(['pid', 'machine_name', 'team_id', 'modified', 'created']);
    foreach ($added_team_ids as $team_id) {
      $query->values([
                       $page_id,
                       $page_machine_name,
                       $team_id,
                       REQUEST_TIME,
                       REQUEST_TIME,
                     ]);
    }

    if ($query->execute() === NULL) {
      drupal_set_message(t("Error updating page team membership"), 'error');
    }
  }

  drupal_set_message(t("Page team membership updated"));
}

/**
 * Page delete submit handler
 */
function _teams_pages_delete_submit_handler(&$form, &$form_state) {
  // Remove all references to deleted page in teams_pages table
  db_delete('teams_pages')
    ->condition('pid', $form_state['page']->subtask['subtask']->pid)
    ->execute();
}

/**
 * Implements hook_preprocess_table().
 *
 * Edit operations displayed on main table for views
 */
function teams_pages_preprocess_table(&$variables) {
  if (current_path() === 'admin/structure/pages') {
    if (teams_user_is_superadmin()) {
      return TRUE;
    }

    $user_teams = teams_get_user_teams();
    if (empty($user_teams)) {
      $accessible_page_machine_names = [];
    }
    else {
      $accessible_page_machine_names = db_select('teams_pages', 'top')
        ->fields('top', ['machine_name'])
        ->condition('top.team_id', $user_teams, 'IN')
        ->execute()
        ->fetchAll(PDO::FETCH_COLUMN, 0);
    }

    // Disable all operations for views user has no access to
    foreach ($variables['rows'] as $row_key => $row) {
      $machine_name = str_replace("page-", "", $row['data']['name']['data']);
      $row['data']['name']['data'] = $machine_name; // @JP - Why are we overwriting $row['data']['name']['data']?
      if (!in_array($machine_name, $accessible_page_machine_names)) {
        $variables['rows'][$row_key]['data']['operations']['data'] = '';
      }
    }
  }
}
