<?php
/**
 * @file
 *
 * Admin file for Teams taxonomy module
 */

/**
 * Implements hook_form_alter().
 *
 * Teams taxonomy form alter used to integrate ownership into admin display
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function teams_taxonomy_form_alter(&$form, &$form_state, $form_id) {
  if (!user_is_logged_in()) {
    return;
  }

  // Make sure this is an taxonomy form
  if ($form_id === 'taxonomy_form_vocabulary' || $form_id === 'taxonomy_form_term') {
    $op = NULL;
    if (isset($form_state['input']['op'])) {
      $op = strtolower($form_state['input']['op']);
    }

    // Don't print the Teams form on the Delete page
    if ($op === 'delete') {
      return;
    }

    $all_teams = [];
    foreach (teams_get_teams() as $team) {
      $all_teams[$team['team_id']] = $team['name'];
    }

    $term_id = NULL;
    if (isset($form['#term']->tid)) {
      $term_id = $form['#term']->tid;
    }
    elseif (isset($form['#term']['tid'])) {
      $term_id = $form['#term']['tid'];
    }

    $vocabulary_id = NULL;
    if (isset($form['#vocabulary']->vid)) {
      $vocabulary_id = $form['#vocabulary']->vid;
    }

    $is_add_taxonomy_page = (!isset($vocabulary_id) && !isset($term_id));
    $is_add_taxonomy_term_page = (isset($vocabulary_id) && !isset($term_id));
    if ($is_add_taxonomy_page || $is_add_taxonomy_term_page) {
      // Add team select options to taxonomy add/edit admin pages
      $module_id = teams_add_form_elements($form, [], 'taxonomy');

      // Disable all teams that you are not a part of and should not be able to select
      $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
      foreach ($disabled_teams as $team_id) {
        $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
      }

      // On add taxonomy form submit create handler
      // Note: using $form['actions']['submit']['#submit'][] will not give you an id on submit
      // The standard $form['#submit'] will return an id on submit
      $form['#submit'][] = '_teams_taxonomy_form_submit_handler';
    }
    // On edit form, display teams that own content without edit ability
    else {
      if (isset($term_id)) {
        $default_checked_teams = _teams_taxonomy_find_term_data_by_tid($term_id, PDO::FETCH_COLUMN);
      }
      elseif (isset($vocabulary_id)) {
        $default_checked_teams = _teams_taxonomy_find_vocabulary_by_vid($vocabulary_id, PDO::FETCH_COLUMN);
      }
      $module_id = teams_add_form_elements($form, $default_checked_teams, 'taxonomy');

      // Only allow the admin user to update teams after node creation
      $is_admin_user = teams_user_is_superadmin();
      if (!$is_admin_user) {
        foreach ($all_teams as $team_id => $team_name) {
          $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
        }
      }
      else {
        $form['#submit'][] = '_teams_taxonomy_form_submit_handler';

        // Prevent the user from modifying the teams for this taxonomy
        $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
        foreach ($disabled_teams as $team_id) {
          $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;

          // Add a hidden input for this checkbox so we still get the form value on submit, but it isn't changeable
          if (in_array($team_id, $default_checked_teams)) {
            $form[$module_id]['teams[' . $team_id . ']'] = [
              '#type' => 'hidden',
              '#value' => $team_id,
            ];
          }
        }
      }
    }
  }
  elseif ($form_id === 'taxonomy_overview_terms') {
    foreach ($form as $row_key => $row) {
      if (is_array($row)) {
        foreach ($row as $key => $term) {
          if (isset($term['tid'])) {
            // Get the teams that have access rights to this type
            $taxonomy_teams = db_select('teams_term_data', 'tax')
              ->fields('tax', ['tid', 'vid', 'team_id'])
              ->condition('tax.tid', $term['tid'], '=')
              ->execute()
              ->fetchAll();

            // Check that the returned result matches with the team
            $permission = teams_has_permission($taxonomy_teams);

            // Remove all unneeded row operations
            if ($permission === FALSE) {
              unset($form[$row_key]['edit']);
            }
          }
        }
      }
    }
  }
}

/**
 * Add or remove teams from this taxonomy node
 *
 * @param array $form The form
 * @param array $form_state The form state
 */
function _teams_taxonomy_form_submit_handler(&$form, &$form_state) {
  $is_taxonomy_term = isset($form_state['values']['tid']);

  if ($is_taxonomy_term) {
    $current_team_ids = _teams_taxonomy_find_term_data_by_tid($form_state['values']['tid'], PDO::FETCH_COLUMN);
  }
  else {
    $current_team_ids = _teams_taxonomy_find_vocabulary_by_vid($form['#vocabulary']->vid, PDO::FETCH_COLUMN);
  }

  $submitted_team_ids = [];
  foreach ($form_state['values']['teams'] as $team_id) {
    if ($team_id !== 0) {
      $submitted_team_ids[] = $team_id;
    }
  }

  $removed_team_ids = array_diff($current_team_ids, $submitted_team_ids);
  if (!empty($removed_team_ids)) {
    if ($is_taxonomy_term) {
      $result = db_delete('teams_term_data')
        ->condition('team_id', $removed_team_ids, 'IN')
        ->condition('tid', $form_state['values']['tid'], '=')
        ->execute();
    }
    else {
      $result = db_delete('teams_vocabulary')
        ->condition('team_id', $removed_team_ids, 'IN')
        ->condition('vid', $form_state['values']['vid'], '=')
        ->execute();
    }

    if ($result === NULL) {
      drupal_set_message(t("Error removing taxonomy teams membership"), 'error');
    }
  }

  $added_team_ids = array_diff($submitted_team_ids, $current_team_ids);
  if (!empty($added_team_ids)) {
    if ($is_taxonomy_term) {
      $query = db_insert('teams_term_data')->fields(['tid', 'vid', 'team_id', 'modified', 'created']);
    }
    else {
      $query = db_insert('teams_vocabulary')->fields(['vid', 'team_id', 'modified', 'created']);
    }

    foreach ($added_team_ids as $team_id) {
      $record = [
        'vid' => $form_state['values']['vid'],
        'team_id' => $team_id,
        'modified' => REQUEST_TIME,
        'created' => REQUEST_TIME,
      ];
      if ($is_taxonomy_term) {
        $record['tid'] = $form_state['values']['tid'];
      }
      $query->values($record);
    }
    if ($query->execute() === NULL) {
      drupal_set_message(t("Error adding taxonomy teams membership"), 'error');
    }
  }

  if ($is_taxonomy_term) {
    $taxonomy_name = arg(3);
    $form_state['redirect'] = '/admin/structure/taxonomy/' . $taxonomy_name;
  }
}

/**
 * Implements hook_taxonomy_term_delete().
 *
 * Cleanup Taxonomy terms in Teams table when deleted
 *
 * @param $term
 */
function teams_taxonomy_taxonomy_term_delete($term) {
  // Remove all references to deleted taxonomy in teams_node_type table
  db_delete('teams_term_data')
    ->condition('tid', $term->tid)
    ->execute();
}

/**
 * Implements hook_taxonomy_vocabulary_delete().
 *
 * Cleanup Taxonomy vocabularies in Teams table when deleted
 *
 * @param $vocabulary
 */
function teams_taxonomy_taxonomy_vocabulary_delete($vocabulary) {
  // Remove all references to deleted taxonomy in teams_node_type table
  db_delete('teams_term_data')
    ->condition('vid', $vocabulary->vid)
    ->execute();

  db_delete('teams_vocabulary')
    ->condition('vid', $vocabulary->vid)
    ->execute();
}

/**
 * Implements hook_preprocess_HOOK
 *
 * Modify taxonomy table to remove operations if access is denied
 *
 * @param $variables
 */
function teams_taxonomy_preprocess_table(&$variables) {
  $is_taxonomy_structure_page = (current_path() === 'admin/structure/taxonomy');
  if ($is_taxonomy_structure_page) {
    if (teams_user_is_superadmin()) {
      return;
    }

    // Check for operations header in vars
    foreach ($variables['header'] as $header_column_number => $header) {
      // Find correct operations array key value
      if (isset($header['data']) && $header['data'] === 'Operations') {
        $operations_column_number = $header_column_number;
        break;
      }
    }

    // remove operations for views without user access rights
    if (isset($operations_column_number)) {
      $user_teams = teams_get_user_teams();
      if (empty($user_teams)) {
        $vocabulary_taxonomy_names = [];
      }
      else {
        // Query all views user has access to based on teams association
        $query = db_select('teams_vocabulary', 'tv');
        $query->join('taxonomy_vocabulary', 'taxv', 'tv.vid = taxv.vid');
        $vocabulary_taxonomy_names = $query->fields('taxv', ['name'])
                                           ->distinct()// @TODO: This table needs a unique key on team_id, vid
                                           ->condition('tv.team_id', $user_teams, 'IN')
                                           ->execute()
                                           ->fetchAll(PDO::FETCH_COLUMN, 0);
      }

      // Disable all operations for views user has no access to
      foreach ($variables['rows'] as $row_key => $row) {
        if (!in_array($row['data'][0], $vocabulary_taxonomy_names)) {
          $variables['rows'][$row_key]['data'][$operations_column_number] = '';
          $variables['rows'][$row_key]['data'][3] = '';
        }
      }
    }
  }
}
