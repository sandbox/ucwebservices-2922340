<?php

/**
 * @file
 *
 * Admin file for Teams Menu module
 */

/**
 * Implements hook_form_alter().
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function teams_menu_form_alter(&$form, &$form_state, $form_id) {
  if (!user_is_logged_in()) {
    return;
  }

  if ($form_id === 'menu_edit_menu' || $form_id === 'menu_edit_item') {
    $all_teams = [];
    foreach (teams_get_teams() as $team) {
      $all_teams[$team['team_id']] = $team['name'];
    }

    $is_menu_item = isset($form['mlid']['#value']);
    if ($is_menu_item) {
      $default_checked_teams = _teams_menu_find_teams_by_link_id($form['mlid']['#value'], PDO::FETCH_COLUMN);
      $is_new = empty($form['mlid']['#value']);
    }
    else {
      $default_checked_teams = _teams_menu_find_teams_by_menu_name($form['old_name']['#value'], PDO::FETCH_COLUMN);
      $is_new = empty($form['old_name']['#value']);
    }

    if ($is_new) {
      // Attach teams display to form as content
      $module_id = teams_add_form_elements($form, '', 'menus');

      $form['#submit'][] = 'teams_menu_form_submit_handler';

      // Disable all teams select editing ability
      $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
      foreach ($disabled_teams as $team_id) {
        $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
      }
    }
    else {
      // Add team select options to menu add/edit admin pages
      $module_id = teams_add_form_elements($form, $default_checked_teams, 'menus');

      // Only allow the admin user to update teams after node creation
      $is_admin_user = teams_user_is_superadmin();
      if (!$is_admin_user) {
        foreach ($all_teams as $team_id => $team_name) {
          $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
        }
      }
      else {
        $form['#submit'][] = 'teams_menu_form_submit_handler';

        // Prevent the user from modifying the teams for this menu
        $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
        foreach ($disabled_teams as $team_id) {
          $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;

          // Add a hidden input for this checkbox so we still get the form value on submit, but it isn't changeable
          if (in_array($team_id, $default_checked_teams)) {
            $form[$module_id]['teams[' . $team_id . ']'] = [
              '#type' => 'hidden',
              '#value' => $team_id,
            ];
          }
        }
      }
    }
  }
  elseif ($form_id === 'menu_overview_form') {
    if (teams_user_is_superadmin()) {
      return;
    }

    $user_teams = teams_get_user_teams();
    if (empty($user_teams)) {
      $menu_teams_access = [];
    }
    else {
      $menu_teams_access = _teams_menu_find_menu_links_by_teams($user_teams);
    }

    foreach ($form as $key => $value) {
      // Create and populate array to hold all teams that have access to current menu for pre select
      if (isset($value['mlid']) && !in_array($value['mlid']['#value'], $menu_teams_access)) {
        $form[$key]['operations']['edit']['#access'] = FALSE;
        if (isset($form[$key]['operations']['delete'])) {
          $form[$key]['operations']['delete']['#access'] = FALSE;
        }
        if (isset($form[$key]['operations']['reset'])) {
          $form[$key]['operations']['reset']['#access'] = FALSE;
        }
      }
    }
  }
}

/**
 * Implements hook_menu_local_tasks_alter().
 *
 * @param array $data The local tasks
 * @param array $router_item The router item
 * @param string $root_path The root path
 */
function teams_menu_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  if ($root_path !== 'admin/structure/menu/manage/%') {
    return;
  }

  if (teams_user_is_superadmin()) {
    return;
  }

  // Remove the "Edit Menu" tab shown on /admin/structure/menu/manage/%menu-name if you don't aren't a superadmin
  foreach ($data['tabs'] as $tab_index => $tab) {
    foreach ($tab['output'] as $output_index => $output) {
      if ($output['#link']['title'] === 'Edit menu') {
        unset($data['tabs'][$tab_index]['output'][$output_index]);
      }
    }
  }
}

/**
 * Menu form submit handler
 *
 * @param array $form The form
 * @param array $form_state The form state
 */
function teams_menu_form_submit_handler($form, &$form_state) {
  $is_menu_item = isset($form['mlid']['#value']);
  if ($is_menu_item) {
    $menu_id = $form['mlid']['#value'];
    $current_team_ids = _teams_menu_find_teams_by_link_id($menu_id, PDO::FETCH_COLUMN);
  }
  else {
    if (!empty($form['old_name']['#value'])) {
      $menu_id = $form['old_name']['#value'];
    }
    else {
      $menu_id = 'menu-' . $form_state['values']['menu_name'];
    }
    $current_team_ids = _teams_menu_find_teams_by_menu_name($menu_id, PDO::FETCH_COLUMN);
  }

  $submitted_team_ids = [];
  foreach ($form_state['values']['teams'] as $team_id) {
    if ($team_id !== 0) {
      $submitted_team_ids[] = $team_id;
    }
  }

  $removed_team_ids = array_diff($current_team_ids, $submitted_team_ids);
  if (!empty($removed_team_ids)) {
    if ($is_menu_item) {
      $result = db_delete('teams_menu_links')
        ->condition('team_id', $removed_team_ids, 'IN')
        ->condition('mlid', $menu_id, '=')
        ->execute();
    }
    else {
      $result = db_delete('teams_menu_custom')
        ->condition('team_id', $removed_team_ids, 'IN')
        ->condition('menu_name', $menu_id, '=')
        ->execute();
    }

    if ($result === NULL) {
      drupal_set_message(t("Error removing teams from menu"), 'error');
    }
  }

  $added_team_ids = array_diff($submitted_team_ids, $current_team_ids);
  if (!empty($added_team_ids)) {
    if ($is_menu_item) {
      $query = db_insert('teams_menu_links')->fields(['mlid', 'team_id', 'modified', 'created']);
    }
    else {
      $query = db_insert('teams_menu_custom')->fields(['menu_name', 'team_id', 'modified', 'created']);
    }

    foreach ($added_team_ids as $team_id) {
      $record = [
        'team_id' => $team_id,
        'modified' => REQUEST_TIME,
        'created' => REQUEST_TIME,
      ];
      if ($is_menu_item) {
        $record['mlid'] = $menu_id;
      }
      else {
        $record['menu_name'] = $menu_id;
      }
      $query->values($record);
    }
    if ($query->execute() === NULL) {
      drupal_set_message(t("Error adding teams to menu"), 'error');
    }
  }
}

/**
 * Implements hook_preprocess_HOOK
 *
 * Modify content types table to remove operations if access is denied
 *
 * @param array $variables The table variables
 */
function teams_menu_preprocess_table(&$variables) {
  $is_menu_structure_page = (current_path() === 'admin/structure/menu');
  if ($is_menu_structure_page) {
    if (teams_user_is_superadmin()) {
      return;
    }

    // Load the list of menus this user has access to
    $user_teams = teams_get_user_teams();
    if (empty($user_teams)) {
      $menu_titles = [];
    }
    else {
      $menu_titles = _teams_menu_find_menu_titles_by_teams($user_teams);
    }

    // Find the "Operations" column index in the table
    foreach ($variables['header'] as $header_column_number => $header) {
      if (isset($header['data']) && $header['data'] === 'Operations') {
        $operations_column_number = $header_column_number;
        break;
      }
    }

    // Remove all menu rows this user has no access for
    if (isset($operations_column_number)) {
      foreach ($variables['rows'] as $row_key => $row) {
        $menu_title = preg_replace('/<.+>/', '', $row[0]);
        if (!in_array($menu_title, $menu_titles)) {
          unset($variables['rows'][$row_key]);
        }
      }
    }
  }
}

