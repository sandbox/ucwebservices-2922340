<?php

/**
 * @file
 *
 * Admin include file used to handle interfacing with the views api
 *
 */

/**
 * Use the views api hook to link to module custom theme folder
 */
function teams_views_api() {
  return [
    'api' => 3,
    'path' => drupal_get_path('module', 'teams') . '/views',
    'template path' => drupal_get_path('module', 'teams') . '/templates',
  ];
}

/**
 * Return to views all custom tables and field information that needs to be avaliable to views
 *
 * @return array
 */
function teams_views_data() {
  // Make Teamss teams table accessable to views
  // The 'group' index will be used as a prefix in the UI for any of this table's fields
  $data['teams_teams']['table']['group'] = t('Teams table');

  // Define this as a base table
  $data['teams_teams']['table']['base'] = [
    'field' => 'team_id',
    'title' => t('Teams - Team List'),
    'help' => t("Team list display based on teams module"),
    'weight' => -10,
  ];

  // Reference to table fields
  $data['teams_teams']['team_id'] = [
    'title' => t('Team Id'),
    'help' => t('Team id field'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
  ];

  $data['teams_teams']['name'] = [
    'title' => t('Name'),
    'help' => t('Team name field'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
    ],
  ];

  $data['teams_teams']['parent_id'] = [
    'title' => t('Parent Id'),
    'help' => t('Parent id field.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
  ];

  $data['teams_teams']['status'] = [
    'title' => t('Status'),
    'help' => t('Team status field'),
    'field' => [
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
    ],
  ];

  $data['teams_teams']['modified'] = [
    'title' => t('Modified'),
    'help' => t('Team last modified field'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
  ];

  $data['teams_teams']['created'] = [
    'title' => t('Created'),
    'help' => t('Team created field'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
  ];

  // Make Teamss users table accessable to views
  // The 'group' index will be used as a prefix in the UI for any of this table's fields
  $data['teams_users']['table']['group'] = t('Teams users association to teams table');

  // Define this as a base table
  $data['teams_users']['table']['base'] = [
    'field' => 'team_user_id',
    'title' => t('Teams - Team User List'),
    'help' => t("Team user list display based on teams module"),
    'weight' => -10,
  ];

  // Reference to table fields
  $data['teams_users']['team_user_id'] = [
    'title' => t('Team User Id'),
    'help' => t('Team user id field'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
  ];

  $data['teams_users']['uid'] = [
    'title' => t('Drupal User Id'),
    'help' => t('Drupal user id field'),
    'relationship' => [
      'base' => 'users',
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User table relationship'),
    ],
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
  ];

  $data['teams_users']['team_id'] = [
    'title' => t('Team Id'),
    'help' => t('Team id field'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'argument' => [
      'handler' => 'views_handler_argument_string',
    ],
  ];

  $data['teams_users']['modified'] = [
    'title' => t('Modified'),
    'help' => t('Date user was last modified in team field'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
  ];

  $data['teams_users']['created'] = [
    'title' => t('Created'),
    'help' => t('Date user was added to team field'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
  ];

  return $data;
}
