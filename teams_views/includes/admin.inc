<?php
/**
 * @file
 *
 * Admin file for Teams views module
 */

/**
 * view access hook used with Teams api to set view level access for teams
 */
function teams_views_form_alter(&$form, &$form_state, $form_id) {
  if (!user_is_logged_in()) {
    return;
  }

  // On add new view form
  if ($form_id === 'views_ui_add_form') {
    // Hide save and exit button to prevent teams ownership issues
    hide($form['save']);
  }

  // Form displayed on add / edit view
  elseif ($form_id === 'views_ui_edit_form') {
    $all_teams = [];
    foreach (teams_get_teams() as $team) {
      $all_teams[$team['team_id']] = $team['name'];
    }

    // Add team select options to node add/edit admin pages
    $module_id = teams_add_form_elements($form, [], 'views', 1000);

    // Check for vid equal to new for new vids
    if ($form_state['view']->vid === 'new') {
      // Disable all teams that you are not a part of, and should not be able to select
      $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
      foreach ($disabled_teams as $team_id) {
        $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
      }

      // Submit handler used to handle new view entrys for teams
      $form['actions']['save']['#submit'][] = '_teams_views_form_submit_handler';
    }
    // On view edit
    else {
      // Check if user has permission to view
      $view_id = $form_state['view']->vid;
      $view_machine_name = $form_state['view']->name;
      $view_teams_access = _teams_entity_find_views_by_vid($view_id, $view_machine_name);
      if (!teams_has_permission($view_teams_access)) {
        _teams_access_denied();
        return;
      }

      $default_checked_teams = _teams_entity_find_views_by_vid($view_id, $view_machine_name, PDO::FETCH_COLUMN);
      $form[$module_id]['teams']['#default_value'] = $default_checked_teams;

      // Only allow the admin user to update teams after node creation
      $is_admin_user = teams_user_is_superadmin();
      if (!$is_admin_user) {
        foreach ($all_teams as $team_id => $team_name) {
          $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
        }
      }
      else {
        $form[$module_id]['save'] = [
          '#type' => 'submit',
          '#value' => 'Save Teams',
          '#submit' => [
            '_teams_views_form_submit_handler',
          ],
        ];

        // Prevent the user from modifying the teams for this entity
        $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
        foreach ($disabled_teams as $team_id) {
          $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;

          // Add a hidden input for this checkbox so we still get the form value on submit, but it isn't changeable
          if (in_array($team_id, $default_checked_teams)) {
            $form['teams_entity_team_select']['teams[' . $team_id . ']'] = [
              '#type' => 'hidden',
              '#value' => $team_id,
            ];
          }
        }
      }
    }
  }
}

/**
 * On new view custom submit handler
 */
function _teams_views_form_submit_handler(&$form, &$form_state) {
  $module_id = 'teams_views_team_select';

  $view_id = (int) $form_state['view']->vid;
  $view_machine_name = $form_state['view']->name;
  $current_team_ids = _teams_entity_find_views_by_vid($view_id, $view_machine_name, PDO::FETCH_COLUMN);

  $submitted_team_ids = [];
  foreach ($form_state['values'][$module_id]['teams'] as $team_id) {
    if ($team_id !== 0) {
      $submitted_team_ids[] = $team_id;
    }
  }

  $removed_team_ids = array_diff($current_team_ids, $submitted_team_ids);
  if (!empty($removed_team_ids)) {
    $result = db_delete('teams_views')
      ->condition('team_id', $removed_team_ids, 'IN')
      ->condition('vid', $view_id, '=')
      ->execute();
    if ($result === NULL) {
      drupal_set_message(t("Error removing teams from view"), 'error');
    }
  }

  $added_team_ids = array_diff($submitted_team_ids, $current_team_ids);
  if (!empty($added_team_ids)) {
    $query = db_insert('teams_views')->fields(['vid', 'machine_name', 'team_id', 'modified', 'created']);
    foreach ($added_team_ids as $team_id) {
      $query->values([
                       $view_id,
                       $view_machine_name,
                       $team_id,
                       REQUEST_TIME,
                       REQUEST_TIME,
                     ]);
    }

    if ($query->execute() === NULL) {
      drupal_set_message(t("Error adding view teams membership"), 'error');
    }
  }
}

/**
 * Edit operations displayed on main table for views
 */
function teams_views_preprocess_table(&$variables) {
  $is_view_structure_page = (current_path() === 'admin/structure/views');
  if ($is_view_structure_page) {
    if (teams_user_is_superadmin()) {
      return;
    }

    // Check for operations header in vars
    foreach ($variables['header'] as $header_column_number => $header) {
      // Find correct operations array key value
      if (isset($header['data']) && $header['data'] === 'Operations') {
        $operations_column_number = $header_column_number;
        break;
      }
    }

    // remove operations for views without user access rights
    if (isset($operations_column_number)) {
      $user_teams = teams_get_user_teams();
      if (empty($user_teams)) {
        $views_with_access_right = [];
      }
      else {
        // Query all views user has access to based on teams association
        $views_with_access_right = db_select('teams_views', 'tov')
          ->fields('tov', ['machine_name'])
          ->condition('tov.team_id', $user_teams, 'IN')
          ->execute()
          ->fetchAll(PDO::FETCH_COLUMN, 0);
      }

      // Disable all operations for views user has no access to
      foreach ($variables['rows'] as $row_key => $row) {
        $row['title'] = str_replace("Machine name: ", "", $row['title']);
        if (!in_array($row['title'], $views_with_access_right)) {
          $variables['rows'][$row_key]['data'][$operations_column_number]['data'] = '';
        }
      }
    }
  }
}
