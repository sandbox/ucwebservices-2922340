<?php
/**
 * @file
 *
 * Admin file for Teams Entity module
 */

/**
 * Implements hook_form_alter().
 *
 * Teams Entity form alter used to integrate ownership into admin display
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function teams_entity_form_alter(&$form, &$form_state, $form_id) {
  if (!user_is_logged_in()) {
    return;
  }

  // Make sure this is an entity form and we're not deleting the entity
  if (isset($form['entity']) && is_array($form['entity']) && strpos($form_id, 'delete') === FALSE) {
    $all_teams = _teams_get_all_teams_form();

    $is_add_page = !isset($form['entity']['#value']->id);
    if ($is_add_page) {

      $module_id = teams_add_form_elements($form, '', 'entity');

      // On add entity form submit create handler
      // Note: using $form['actions']['submit']['#submit'][] will not give you an id on submit
      // The standard $form['#submit'] will return an id on submit
      $form['#submit'][] = '_teams_entity_form_submit_handler';

      // Disable all teams that you are not a part of, and should not be able to select
      $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
      foreach ($disabled_teams as $team_id) {
        $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
      }
    }
    // On edit form, display teams that own content without edit ability
    elseif (isset($form['entity']['#value']->id)) {

      // Get the teams that have access rights to this entity
      $default_checked_teams = _teams_entity_find_entities_by_eid($form['entity']['#value']->id, PDO::FETCH_COLUMN);

      $module_id = teams_add_form_elements($form, $default_checked_teams, 'entity');

      // Only allow the admin user to update teams after node creation
      $is_admin_user = teams_user_is_superadmin();
      if (!$is_admin_user) {
        foreach ($all_teams as $team_id => $team_name) {
          $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;
        }
      } else {
        $form['#submit'][] = '_teams_entity_form_submit_handler';

        // Prevent the user from modifying the teams for this entity
        $disabled_teams = array_diff(array_keys($all_teams), teams_get_user_teams());
        foreach ($disabled_teams as $team_id) {
          $form[$module_id]['teams'][$team_id]['#disabled'] = TRUE;


          // Add a hidden input for this checkbox so we still get the form value on submit, but it isn't changeable
          if (in_array($team_id, $default_checked_teams)) {
            $form[$module_id]['teams[' . $team_id . ']'] = [
              '#type' => 'hidden',
              '#value' => $team_id,
            ];
          }
        }
      }
    }
  }
}

/**
 * Entity form submit
 *
 * @param $form
 * @param $form_state
 */
function _teams_entity_form_submit_handler(&$form, &$form_state) {
  $entity_id = $form['entity']['#value']->id;
  $current_team_ids = _teams_entity_find_entities_by_eid($entity_id, PDO::FETCH_COLUMN);

  $submitted_team_ids = [];
  foreach ($form_state['values']['teams'] as $team_id) {
    if ($team_id !== 0) {
      $submitted_team_ids[] = $team_id;
    }
  }

  $removed_team_ids = array_diff($current_team_ids, $submitted_team_ids);
  if (!empty($removed_team_ids)) {
    $result = db_delete('teams_entity')
      ->condition('team_id', $removed_team_ids, 'IN')
      ->condition('entity_id', $entity_id, '=')
      ->execute();
    if ($result === NULL) {
      drupal_set_message(t("Error removing teams from entity"), 'error');
    }
  }

  $added_team_ids = array_diff($submitted_team_ids, $current_team_ids);
  if (!empty($added_team_ids)) {
    $query = db_insert('teams_entity')->fields(['entity_id', 'team_id', 'modified', 'created']);

    foreach ($added_team_ids as $team_id) {
      $query->values([
                       $entity_id,
                       $team_id,
                       REQUEST_TIME,
                       REQUEST_TIME,
                     ]);
    }

    if ($query->execute() === NULL) {
      drupal_set_message(t("Error adding entity teams membership"), 'error');
    }
  }
}

/**
 * Implements hook_entity_delete().
 *
 * Cleanup Entities in Teams table when deleted
 *
 * @param $entity
 * @param $type
 */
function teams_entity_delete($entity, $type) {
  // Make sure we're deleting an entity, and not a regular node
  if (!isset($entity->id)) {
    return;
  }

  db_delete('teams_entity')
    ->condition('entity_id', $entity->id)
    ->execute();
}
