<?php

/**
 * @file
 *
 * Teams Entity module
 */

module_load_include('inc', 'teams_entity', 'includes/admin');

/**
 * Implements hook_entity_info_alter().
 *
 * Get all entities
 *
 * @param $entity_info
 */
function teams_entity_entity_info_alter(&$entity_info) {
  $entities = entity_get_info();
  foreach ($entities as $entity_key => $entity) {
    if (isset($entity['entity class']) && isset($entity['controller class']) && strpos($entity['controller class'], 'Entity') !== FALSE) {
      $entity_info[$entity_key]['access callback'] = 'teams_entity_access';
    }
  }
}

/**
 * Implements hook_access().
 *
 * Access Callback for Entity info
 *
 * @param string $op The operation
 * @param mixed $entity The entity object, or string.
 * @param string $account $account (optional) The account to check, if not given use currently logged in user.
 *
 * @return bool
 */
function teams_entity_access($op, $entity = NULL, $account = NULL) {
  if (!user_is_logged_in()) {
    return FALSE;
  }

  if (teams_user_is_superadmin()) {
    return TRUE;
  }

  if ($op === 'view' && user_access('eck list entity types', $account)) {
    return TRUE;
  }

  // Handle "Add X" links.
  if (is_string($entity) && $op === "create" && user_access('eck add entity types', $account)) {
    return TRUE;
  }

  if (in_array($op, ['update', 'create', 'delete'])) {
    $permission_action = $op;
    if ($op === 'update') {
      $permission_action = 'edit';
    } elseif ($op === 'create') {
      $permission_action = 'add';
    }
    if (isset($entity->id) && user_access('eck ' . $permission_action . ' entity types', $account)) {
      $entity_teams_access = _teams_entity_find_entities_by_eid($entity->id);
      if (!empty($entity_teams_access)) {
        return teams_has_permission($entity_teams_access);
      }
    }
  }

  return FALSE;
}

/**
 * Get the teams that have access rights to this node
 *
 * @param integer $eid The node id
 * @param boolean $fetch_mode The PDO fetch mode. Optional
 *
 * @return array
 */
function _teams_entity_find_entities_by_eid($eid, $fetch_mode = NULL) {
  return db_select('teams_entity', 'toe')
    ->fields('toe', ['team_id'])
    ->condition('toe.entity_id', $eid, '=')
    ->execute()
    ->fetchAll($fetch_mode);
}
