<?php

/**
 * Views include hook that registers custom fields in the system. This is needed to use any tables that are
 * not inherently available in Views.
 *
 * @return array hook data
 */
function teams_handlers() {
    return array(
        'info' => array(
            'path' => drupal_get_path('module', 'teams') . '/views',
        )
    );
}